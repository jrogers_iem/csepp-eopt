﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Microsoft.SharePoint;
using Microsoft.Office.DocumentManagement;
using Microsoft.Office.DocumentManagement.DocumentSets;

namespace IEM.EOPT.Util
{
    internal class ManageSectionUtil : BaseUtil
    {
        #region Constructor
        internal ManageSectionUtil() { }
        #endregion

        #region Add Section
        /// <summary>
        /// Adds a File to the specidfied Folder.
        /// </summary>
        /// <param name="Web"></param>
        /// <param name="FolderId"></param>
        /// <param name="Name"></param>
        /// <param name="Group"></param>
        /// <param name="Order"></param>
        /// <param name="FileName"></param>
        /// <param name="FileBytes"></param>
        internal void AddSection(SPWeb Web, Guid FolderId, String Name, String Group, int Order, string FileName, byte[] FileBytes)
        {
            try
            {
                //Get the correct Folder...
                SPFolder Folder = Web.GetFolder(FolderId);
                //Get the coresponding DocumentSet...
                DocumentSet Set = DocumentSet.GetDocumentSet(Folder);
                //Add the File...
                SPFile File = Set.Folder.Files.Add(FileName, FileBytes, true);
                //Get the Item...
                SPListItem Item = File.Item;
                //Get the Content Type...
                SPContentType PS = Set.ParentList.ContentTypes["Plan Section"];
                //Now set the Properties...
                Item[SPBuiltInFieldId.ContentType] = PS;
                Item[SPBuiltInFieldId.ContentTypeId] = PS.Id;
                Item[SPBuiltInFieldId.Title] = FileName;
                //Item[SPBuiltInFieldId.Name] = FileName;
                Item[PlanSectionFieldId._PlanSectionOrder] = Order;
                Item[PlanSectionFieldId._PlanSectionGroup] = Group;
                Item[PlanSectionFieldId._PlanSectionCategoryOrder] = GetPlanSectionCategoryOrder(Group);
                Item[PlanSectionFieldId._PlanSectionHeaderLine1] = Item.ParentList.Title;
                Item[PlanSectionFieldId._PlanSectionStatus] = "Not Started";
                Item[PlanSectionFieldId._PlanSectionNIMSConsistent] = "No";
                //Run an update...
                Item.SystemUpdate();
                //Update the File...
                File.Update();
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.AddSection");
                throw ex;
            }
        }
        #endregion

        #region Update Section
        /// <summary>
        /// Updates the Plan Section with the values provided.
        /// </summary>
        /// <param name="Web"></param>
        /// <param name="ListId"></param>
        /// <param name="ItemId"></param>
        /// <param name="Group"></param>
        /// <param name="Order"></param>
        internal void UpdateSection(SPWeb Web, String ListId, String ItemId, String Title, String Group, int Order)
        {
            try
            {
                //Get the List...
                SPList List = Web.Lists[new Guid(ListId)];
                //Get the Item...
                SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(ItemId));
                //Update the Fields...
                Item[SPBuiltInFieldId.Title] = Title;
                Item[PlanSectionFieldId._PlanSectionGroup] = Group;
                Item[PlanSectionFieldId._PlanSectionOrder] = Order;
                Item[PlanSectionFieldId._PlanSectionCategoryOrder] = GetPlanSectionCategoryOrder(Group);
                //Save the changes...
                Item.Update();
            }
            catch (Exception ex) { HandleException(ex, "ManageSectionUtil.EditSection"); throw ex; }
        }
        #endregion

        #region Move Section
        /// <summary>
        /// Moves the Section down in the list order.
        /// </summary>
        /// <param name="SiteUrl"></param>
        /// <param name="ListId"></param>
        /// <param name="SelectedItemId"></param>
        internal void MoveSectionDown(String SiteUrl, String ListId, String SelectedItemId, String Folder)
        {
            try
            {
                //Get the Site...
                using (SPSite Site = new SPSite(SiteUrl))
                {   //Open the Web...
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        //Since this runs during a page load, set this to true...
                        Web.AllowUnsafeUpdates = true;
                        //Get the List...
                        SPList List = Web.Lists[new Guid(ListId)];
                        //Get the Item...
                        SPListItem Item = List.GetItemById(Int32.Parse(SelectedItemId));
                        //Reorder...
                        #region Borrowed Code
                        //I took this from the legacy system - MoveSectionDown.aspx / Page_Load
                        int movePos = int.Parse(Item["Plan Section Order"].ToString());
                        string moveGroup = Item["Plan Section Group"].ToString();
                        string sectionTitle = Item["Title"].ToString();

                        int maxPos = 0;
                        int minPos = 10000;
                        int swapPosition = 0;
                        bool lastItem = false;

                        //get max and min
                        SPListItemCollection Items = GetItems(List, Folder);

                        foreach (SPListItem section in Items) //List.Items)
                        {
                            int pos = int.Parse(section["Plan Section Order"].ToString());
                            if (pos > maxPos) maxPos = pos;
                            if (pos < minPos) minPos = pos;
                        }

                        swapPosition = movePos;

                        SortedList sectionOrders = new SortedList();

                        Items = GetItems(List, Folder);
                        //get the first position bigger than the movePos
                        foreach (SPListItem section in Items)//List.Items)
                        {
                            if (section["Plan Section Group"].ToString() == moveGroup)
                            {
                                int pos = int.Parse(section["Plan Section Order"].ToString());
                                if (pos > movePos)
                                {
                                    //only add it if it does not already exist
                                    //It would already exist if there are multiple sections with the same section order
                                    //normally this wouldn't happen unless the users edit the section orders manually and create duplicate section orders
                                    if (!sectionOrders.Contains(pos))
                                    {
                                        sectionOrders.Add(pos, pos);
                                    }
                                }
                            }
                        }

                        //the swap position will be the first item in this sorted list since we're looking for the smallest number
                        //that is greater than the movePos
                        if (sectionOrders.Count > 0)
                        {
                            lastItem = false;
                            swapPosition = int.Parse(sectionOrders.GetByIndex(0).ToString());
                        }
                        else
                        {
                            //if there are no more items in this list above the one we're moving, this is the last item and thus
                            //moving this item down does not make sense
                            lastItem = true;
                        }

                        //move sections
                        if ((movePos < maxPos) && (lastItem == false))
                        {
                            Items = GetItems(List, Folder);

                            foreach (SPListItem section in Items)// List.Items)
                            {
                                if (section["Plan Section Group"].ToString() == moveGroup)
                                {
                                    int pos = int.Parse(section["Plan Section Order"].ToString());
                                    if (pos == swapPosition) section["Plan Section Order"] = movePos.ToString();
                                    int newPos = movePos + 1;
                                    if ((pos == movePos) && (section["Title"].ToString() == sectionTitle)) section["Plan Section Order"] = newPos.ToString();
                                    section.SystemUpdate(false);
                                }
                            }
                        }
                        #endregion
                        //Save the changes...
                        Item.Update();
                        //Set the valuse back...
                        Web.AllowUnsafeUpdates = false;
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.MoveSection");
                throw ex;
            }
        }
        /// <summary>
        /// Moves the section up in the list order.
        /// </summary>
        /// <param name="SiteUrl"></param>
        /// <param name="ListId"></param>
        /// <param name="SelectedItemId"></param>
        internal void MoveSectionUp(String SiteUrl, String ListId, String SelectedItemId, String Folder)
        {
            try
            {
                //Get the Site...
                using (SPSite Site = new SPSite(SiteUrl))
                {   //Open the Web...
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        //Since this runs during page load, set this to true...
                        Web.AllowUnsafeUpdates = true;
                        //Get the List...
                        SPList List = Web.Lists[new Guid(ListId)];
                        //Get the Item...
                        SPListItem Item = List.GetItemById(Int32.Parse(SelectedItemId));
                        //Reorder...
                        #region Borrowed Code
                        //I took this from the legacy system - MoveSectionUp.aspx / Page_Load
                        int movePos = int.Parse(Item["Plan Section Order"].ToString());
                        string moveGroup = Item["Plan Section Group"].ToString();
                        string sectionTitle = Item["Title"].ToString();

                        Web.AllowUnsafeUpdates = true;

                        int maxPos = 0;
                        int swapPosition = 0;
                        int minPos = 10000;
                        SPListItemCollection Items = GetItems(List, Folder);
                        //get max and min
                        foreach (SPListItem section in Items)//List.Items)
                        {
                            int pos = int.Parse(section["Plan Section Order"].ToString());
                            if (pos > maxPos) maxPos = pos;
                            if (pos < minPos) minPos = pos;
                        }

                        swapPosition = movePos;

                        SortedList sectionOrders = new SortedList();

                        Items = GetItems(List, Folder);
                        //get the first position smaller than the movePos
                        foreach (SPListItem section in Items)// List.Items)
                        {
                            if (section["Plan Section Group"].ToString() == moveGroup)
                            {
                                int pos = int.Parse(section["Plan Section Order"].ToString());
                                if (pos < movePos)
                                {
                                    //only add it if it does not already exist
                                    //It would already exist if there are multiple sections with the same section order
                                    //normally this wouldn't happen unless the users edit the section orders manually and create duplicate section orders
                                    if (!sectionOrders.Contains(pos))
                                    {
                                        sectionOrders.Add(pos, pos);
                                    }
                                }
                            }
                        }

                        if (sectionOrders.Count > 0)
                        {
                            swapPosition = int.Parse(sectionOrders.GetByIndex(sectionOrders.Values.Count - 1).ToString());
                        }

                        //move sections
                        if (movePos > minPos && movePos > 0)
                        {
                            Items = GetItems(List, Folder);
                            foreach (SPListItem section in Items)//List.Items)
                            {
                                if (section["Plan Section Group"].ToString() == moveGroup)
                                {
                                    int pos = int.Parse(section["Plan Section Order"].ToString());
                                    if (pos == swapPosition) section["Plan Section Order"] = (swapPosition + 1).ToString();

                                    if ((pos == movePos) && (section["Title"].ToString() == sectionTitle)) section["Plan Section Order"] = swapPosition.ToString();
                                    section.SystemUpdate(false);

                                }
                            }
                        }
                        #endregion
                        //Save the changes...
                        Item.Update();
                        //Set the valuse back...
                        Web.AllowUnsafeUpdates = false;
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.MoveSectionUp");
                throw ex;
            }
        }
        #endregion
                
        #region Section Status
        /// <summary>
        /// Sets the Plan Section Status to the input provided.
        /// </summary>
        /// <param name="ListId"></param>
        /// <param name="ItemId"></param>
        /// <param name="Status"></param>
        internal void SetSectionStatus(String ListId, String ItemId, String Status)
        {
            try
            {
                //Run elevated until we figure out permissions... this shoulf be JSOM...
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    //Get the Site...
                    using (SPSite Site = SPContext.Current.Site)
                    {   //Open the Web...
                        using (SPWeb Web = Site.OpenWeb())
                        {   //This happens on page load, so set unsafe updates...
                            Web.AllowUnsafeUpdates = true;
                            //Get the List...
                            SPList List = Web.Lists.GetList(new Guid(ListId), false);
                            //Get the Item...
                            SPListItem Item = List.GetItemById(Int32.Parse(ItemId));
                            //Check for the Field...
                            if (Item.Fields.ContainsField(PlanSectionFieldId._PlanSectionStatus))
                            {   //Set the Status...
                                Item[PlanSectionFieldId._PlanSectionStatus] = Status;
                            }
                            //Commit the changes...
                            Item.Update();
                            //Reset the flag...
                            Web.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.SetSectionStatus");
                throw ex;
            }
        }

        /// <summary>
        /// Flips the NIMS Status if it's not set to N/A.
        /// </summary>
        /// <param name="ListId"></param>
        /// <param name="ItemId"></param>
        internal void ToggleNIMSStatus(String ListId, String ItemId)
        {
            try
            {
                //Run elevated until we figure out permissions... this should be JSOM...
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    //Get the Site...
                    using (SPSite Site = SPContext.Current.Site)
                    {   //Open the Web...
                        using (SPWeb Web = Site.OpenWeb())
                        {   //This happens on page load, so set unsafe updates...
                            Web.AllowUnsafeUpdates = true;
                            //Get the List...
                            SPList List = Web.Lists.GetList(new Guid(ListId), false);
                            //Get the Item...
                            SPListItem Item = List.GetItemById(Int32.Parse(ItemId));
                            //Check for the Field...
                            if (Item.Fields.ContainsField(PlanSectionFieldId._PlanSectionNIMSConsistent))
                            {   //Set the Status...
                                String NIMS = Item.GetFormattedValue(PlanSectionFieldId._PlanSectionNIMSConsistent);
                                String NewValue = String.Empty;
                                //If it's N/A, we skip...
                                if (NIMS != @"N\A")
                                {
                                    if (NIMS.Contains("Yes")) { NewValue = "No"; }
                                    if (NIMS.Contains("No")) { NewValue = "Yes"; }
                                }
                                //Set the NewValue...
                                Item[PlanSectionFieldId._PlanSectionNIMSConsistent] = NewValue;
                            }
                            //Commit the changes...
                            Item.Update();
                            //Reset the flag...
                            Web.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.ToggleNIMSStatus");
                throw ex;
            }
        }
        #endregion

        #region GetNextSectionOrder
        /// <summary>
        /// Returns the next section order for the provided group.
        /// </summary>
        /// <param name="SiteUrl"></param>
        /// <param name="ListId"></param>
        /// <param name="FolderParam"></param>
        /// <param name="Group"></param>
        /// <returns></returns>
        internal int GetNextSectionOrder(String SiteUrl, String ListId, String FolderParam, String Group)
        {
            try
            {
                using (SPSite Site = new SPSite(SiteUrl))
                {
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        SPList List = Web.Lists[new Guid(ListId)];
                        int Index = FolderParam.LastIndexOf(@"/");
                        FolderParam = FolderParam.Substring(Index + 1);

                        SPQuery Query = new SPQuery();
                        //Define the Query...
                        Query.Query = @"<OrderBy>
                                      <FieldRef Name='PlanSectionOrder' Ascending='FALSE' />
                                    </OrderBy>
                                    <Where>
                                      <Eq>
                                        <FieldRef Name='PlanSectionGroup' />
                                        <Value Type='Text'>" + Group + @"</Value>
                                      </Eq>
                                    </Where>";

                        //Set the ViewFields...
                        Query.ViewFields = @"<FieldRef Name='Title' /><FieldRef Name='PlanSectionCategoryOrder' /><FieldRef Name='PlanSectionOrder' />";
                        //Getting the folder object from the list 
                        SPFolder Folder = List.RootFolder.SubFolders[FolderParam];
                        //Set the Folder property
                        Query.Folder = Folder;
                        //Define the return value...
                        int Order = 1;
                        //Get the Items...
                        SPListItemCollection Items = List.GetItems(Query);
                        //Make sure we got something back...
                        if (Items.Count > 0)
                        {
                            //We only care about the first one...
                            SPListItem Item = Items[0];
                            //Now get the next one in line...
                            String Value = Item.GetFormattedValue(PlanSectionFieldId._PlanSectionOrder);
                            Order = Int32.Parse(Value);
                        }
                        //Increment it...
                        Order++;

                        return Order;
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, "ManageSectionUtil.GetNextSectionOrder");
                throw ex;
            }            
        }
        #endregion
    }
}
