﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;

namespace IEM.EOPT.Util
{
    internal sealed class RequestApprovalUtil : BaseUtil
    {
        #region Get Workflow Association
        /// <summary>
        /// Gets the Workflow Association.
        /// </summary>
        /// <param name="Item"></param>
        /// <param name="WorkflowAssociationName"></param>
        /// <returns></returns>
        private SPWorkflowAssociation GetWorkflowAssociationByName(SPListItem Item, string WorkflowAssociationName)
        {
            return Item.ContentType.WorkflowAssociations.GetAssociationByName(WorkflowAssociationName, new System.Globalization.CultureInfo("en-US"));
        }
        #endregion

        #region Start Workflow
        /// <summary>
        /// Starts the Workflow on the Item provided.
        /// </summary>
        /// <param name="SiteUrl"></param>
        /// <param name="ListId"></param>
        /// <param name="ItemId"></param>
        /// <param name="NumberOfApprovers"></param>
        /// <param name="AssociationName"></param>
        internal void StartWorkflow(String SiteUrl, String ListId, String ItemId, int NumberOfApprovers, string AssociationName)
        {
            try
            {
                using (SPSite Site = new SPSite(SiteUrl))
                {
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        SPList List = Web.Lists[new Guid(ListId)];

                        SPListItem Item = List.GetItemById(Int32.Parse(ItemId));

                        //get workflow association object
                        SPWorkflowAssociation WorkflowAssociation = GetWorkflowAssociationByName(Item, AssociationName);

                        if (WorkflowAssociation == null)
                            throw new Exception(string.Format("Workflow association '{0}' could not be found in the list '{1}'", AssociationName, Item.ParentList.Title));

                        WorkflowParametersUtil Util = new WorkflowParametersUtil();
                        
                        Util.NumberOfApprovers = NumberOfApprovers;

                        WorkflowAssociation.AssociationData = Util.getInitXmlString(Util);
                        //start your workflow...
                        Item.Web.Site.WorkflowManager.StartWorkflow(Item, WorkflowAssociation, WorkflowAssociation.AssociationData);
                    }
                }
                
                
            }
            catch (Exception ex)
            {
                base.HandleException(ex, "RequestApprovalUtil");
                throw ex;
            }
        }
        #endregion
    }
}
