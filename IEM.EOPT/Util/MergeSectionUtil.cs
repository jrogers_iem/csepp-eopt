﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.IO;
using Aspose.Words;

namespace IEM.EOPT.Util
{
    internal sealed class MergeSectionUtil : BaseUtil
    {
        #region Enum
        public enum PageNumberTypes
        {
            RomanNumerials = 1,
            Arabic = 2
        }

        public enum TemplateTypes
        {
            basic_plan = 1,
            esf = 2,
            support_annex = 3
        }
        #endregion

        #region Constructors
        internal MergeSectionUtil() { }

        #endregion

        #region Ported Code
        /*  This code was ported from the old project.
         *  It has been modified to decouple it from the UI.
         *  The original unaltered code is located below in a region called Commented Code.
         *  It has also been modified to write the file into a Momory Stream (rather than disk), and place the 
         *  final result in the Document Set in Plan Section Group '(00) Complete Plan'.
         */
        private Document mDocument;
        private string mDocumenNameandPath;
        private string mLocationOfLicenseAndTemplateFile;

        #region Initialize Document
        public void InitializeDocument(string FilePathandSpec, TemplateTypes TemplateType, string LocationOfLicenseAndFormattingFile)
        {
            if (String.IsNullOrEmpty(FilePathandSpec))
                throw new ArgumentException("FilePathandSpec is null or empty.", "FilePathandSpec");

            if (String.IsNullOrEmpty(LocationOfLicenseAndFormattingFile))
                throw new ArgumentException("LocationOfLicenseAndTemplateFile is null or empty.", "LocationOfLicenseAndTemplateFile");

            mDocumenNameandPath = FilePathandSpec;
            mLocationOfLicenseAndTemplateFile = LocationOfLicenseAndFormattingFile;
            FindAndApplyLicense();

            CreateDocument(TemplateType);
        }
        #endregion

        #region Add Document
        public void AddDocument(string DocumentNameandPath, string HeaderLine1, string HeaderLine2, string FooterText, Boolean AddPageNumbers,
            Boolean BeginNewPageNumbers, string PageNumberPreText, PageNumberTypes PageNumbering, bool keepSourceFormatting)
        {
            if (String.IsNullOrEmpty(DocumentNameandPath))
                throw new ArgumentException("DocumentNameandPath is null or empty.", "DocumentNameandPath");

            // Open the source document.
            Document srcDoc = new Document(DocumentNameandPath);
            DocumentBuilder builder = new DocumentBuilder(srcDoc);

            //if (FooterText.Length > 0)
            //{
            for (int sectIdx = 0; sectIdx < srcDoc.Sections.Count; sectIdx++)
            {
                //move documentbuilder cursor to the current section
                builder.MoveToSection(sectIdx);

                Section currentSection = builder.CurrentSection;
                PageSetup pageSetup = currentSection.PageSetup;
                if(!keepSourceFormatting)
                {
                currentSection.ClearHeadersFooters();

                builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);

                builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                // Set font properties for header text.
                builder.Font.Name = "Arial";
                builder.Font.Bold = true;
                builder.Font.Italic = true;
                builder.Font.Size = 9;

                if (HeaderLine1.Length > 0)
                    builder.Write(HeaderLine1);

                if (HeaderLine2.Length > 0)
                {
                    builder.Write("\n");
                    builder.Write(HeaderLine2);
                }

                //move documentbuilder cursor to the footer
                builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary);

                builder.StartTable();

                // Calculate table width as total page width with left and right marins subtracted.
                double tableWidth = pageSetup.PageWidth - pageSetup.LeftMargin - pageSetup.RightMargin;

                builder.InsertCell();
                builder.CellFormat.Width = tableWidth * 2 / 3;

                builder.Font.Name = "Arial";
                builder.Font.Bold = true;
                builder.Font.Italic = true;
                builder.Font.Size = 9;

                //Insert text into teh footer
                builder.Write(FooterText);

                // Align this text to the left.
                builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Left;

                builder.InsertCell();
                builder.CellFormat.Width = tableWidth / 3;

                if (AddPageNumbers == true)
                {
                    builder.Write(PageNumberPreText);
                    builder.InsertField("PAGE", "");
                    Section section = srcDoc.Sections[0];

                    // Set first section page numbering.
                    if (BeginNewPageNumbers == true)
                    {
                        section.PageSetup.RestartPageNumbering = true;
                        section.PageSetup.PageStartingNumber = 1;

                    }
                    if (PageNumbering == PageNumberTypes.RomanNumerials)
                        section.PageSetup.PageNumberStyle = NumberStyle.LowercaseRoman;
                    else
                        section.PageSetup.PageNumberStyle = NumberStyle.Arabic;
                }

                // Align this text to the right.
                builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Right;

                builder.EndRow();
                builder.EndTable();
                }
            }
            //}

            if (keepSourceFormatting)
            {
                //mDocument.Document.AppendDocument(srcDoc, ImportFormatMode.KeepSourceFormatting);
                mDocument.AppendDocument(srcDoc, ImportFormatMode.KeepSourceFormatting);
            }
            else
            {
                //mDocument.Document.AppendDocument(srcDoc, ImportFormatMode.UseDestinationStyles);
                mDocument.AppendDocument(srcDoc, ImportFormatMode.UseDestinationStyles);
               
            }
        }
        #endregion

        #region Finalize Document
        public void FinalizeDocument()
        {
            try
            {
                mDocument.Save(mDocumenNameandPath);
            }
            catch
            {
                // MessageBox.Show("An Error occured while trying to save the merged document, Please make sure the document is not open and try again.");
            }
        }
        #endregion
        
        #region License
        /// <summary>
        /// Search for Aspose.Words and Aspose.Pdf licenses in the application directory.
        /// The File.Exists check is only needed in this demo so it will work
        /// both when the license file is missing and when it is present.
        /// In your real application you just need to call SetLicense.
        /// </summary>
        private void FindAndApplyLicense()
        {
            // Try to find Aspose.Custom license.
            //string licenseFile = mLocationOfLicenseAndTemplateFile + "Aspose.Custom.lic";
            string licenseFile = mLocationOfLicenseAndTemplateFile + "Aspose.Words.lic";
            if (File.Exists(licenseFile))
            {
                LicenseAsposeWords(licenseFile);
                //LicenseAsposePdf(licenseFile);
                return;
            }
        }

        /// <summary>
        /// This code activates Aspose.Words license.
        /// If you don't specify a license, Aspose.Words will work in evaluation mode.
        /// </summary>
        private void LicenseAsposeWords(string licenseFile)
        {
            Aspose.Words.License licenseWords = new Aspose.Words.License();
            licenseWords.SetLicense(licenseFile);
        }

        /// <summary>
        /// This code activates Aspose.Pdf license.
        /// If you don't specify a license, Aspose.Pdf will work in evaluation mode.
        /// </summary>
        private void LicenseAsposePdf(string licenseFile)
        {
            //Aspose.Pdf.License licensePdf = new Aspose.Pdf.License();
            //licensePdf.SetLicense(licenseFile);
        }
        #endregion

        #region Create Document
        private void CreateDocument(TemplateTypes DocumentType)
        {
            try
            {

                string TemplateName = "";

                switch (DocumentType)
                {
                    case TemplateTypes.basic_plan:
                        TemplateName = "basic_plan.doc";
                        break;
                    case TemplateTypes.esf:
                        TemplateName = "esf.doc";
                        break;
                    case TemplateTypes.support_annex:
                        TemplateName = "support_annex.doc";

                        break;
                }

                // Loads the document into Aspose.Words object model.

                mDocument = new Document(mLocationOfLicenseAndTemplateFile + TemplateName);
                mDocument.Sections.Clear();
            }
            catch (Exception ex)
            {
                base.HandleException(ex, "CreateDocument");
                //Logger.LogMessageToFile(ex.InnerException.ToString());
            }


        }
        #endregion

        #region Merge Plan Sections
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ListId"></param>
        /// <param name="Folder"></param>
        /// <param name="KeepSourceFormatting"></param>
        internal void MergePlanSections(String ListId, String Folder, bool KeepSourceFormatting)
        {


            if (KeepSourceFormatting)
            {

                #region KeepSourceFormatting
                string listId = ListId;

                try
                {

                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite site = new SPSite(SPControl.GetContextSite(HttpContext.Current).ID))
                        {
                            SPWeb web = SPControl.GetContextWeb(HttpContext.Current);
                            SPUser user = SPControl.GetContextWeb(HttpContext.Current).CurrentUser;
                            SPSite siteCollection = SPContext.Current.Site;

                            Guid listGuid = new Guid(listId);
                            SPList list = web.Lists[listGuid];

                            //linkListUrl.NavigateUrl = list.DefaultViewUrl;

                            Guid dirGuid = Guid.NewGuid();
                            string dirName = "c:\\temp\\eopt\\" + dirGuid.ToString() + "\\";
                            Directory.CreateDirectory(dirName);

                            //Hashtable htHdr = new Hashtable();
                            //Hashtable ht = new Hashtable();
                            Hashtable htPN = new Hashtable();
                            Hashtable htShowPn = new Hashtable();
                            Hashtable htRestart = new Hashtable();
                            Hashtable htType = new Hashtable();

                            //if this list does not contain the "IsMergedDocument" flag then we need to add it
                            if (!list.Fields.ContainsField("IsMergedDocument"))
                            {
                                web.AllowUnsafeUpdates = true;
                                list.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);
                                list.Update();
                                web.AllowUnsafeUpdates = false;
                            }


                            //if the "Section Group" drop down does not contain the value of "Merged Documents" in its selections
                            //then we need to add this option to the selection
                            if (list.Fields.ContainsField("Plan Section Section Group"))
                            {
                                SPFieldChoice ddlSectionGroup = (SPFieldChoice)list.Fields["Plan Section Section Group"];

                                if (!ddlSectionGroup.Choices.Contains("Merged Documents"))
                                {
                                    web.AllowUnsafeUpdates = true;
                                    ddlSectionGroup.Choices.Add("Merged Documents");
                                    ddlSectionGroup.Update();
                                    list.Update();
                                    web.AllowUnsafeUpdates = false;
                                }
                            }

                            //if the "Section Status" drop down does not contain the value of "Completed" in its selections
                            //then we need to add this option to the selection
                            if (list.Fields.ContainsField("Plan Section Section Status"))
                            {
                                SPFieldChoice ddlSectionStatus = (SPFieldChoice)list.Fields["Plan Section Section Status"];
                                if (!ddlSectionStatus.Choices.Contains("Complete"))
                                {
                                    web.AllowUnsafeUpdates = true;
                                    ddlSectionStatus.Choices.Add("Complete");
                                    ddlSectionStatus.Update();
                                    list.Update();
                                    web.AllowUnsafeUpdates = false;
                                }
                            }

                            SPListItemCollection Items = base.GetItemsMergeFields(list, Folder);

                            foreach (SPListItem item in Items)//list.Items)
                            {
                                SPFile file = item.File;

                                string section = "NoSection";
                                if (item["Plan Section Group"] != null)
                                {
                                    string temp = item["Plan Section Group"].ToString();

                                    temp = temp.Replace("(0", "");
                                    temp = temp.Replace(")", "");

                                    //string section = (string)item["Section Group"].ToString().Substring(1, 1).PadLeft(3, char.Parse("0"));
                                    section = temp.Substring(0, 1).PadLeft(3, char.Parse("0"));
                                }


                                string order = (string)item["Plan Section Order"].ToString().PadLeft(3, char.Parse("0"));
                                string fileName = dirName + section + order + "_" + file.Name;

                                //if (item[PlanSectionFieldId._PlanSectionHeaderLine1] != null)
                                //    htHdr.Add(fileName, item[PlanSectionFieldId._PlanSectionHeaderLine1].ToString());

                                //if (item[PlanSectionFieldId._PlanSectionHeaderLine2] != null)
                                //    ht.Add(fileName, item[PlanSectionFieldId._PlanSectionHeaderLine2].ToString());

                                if (item[PlanSectionFieldId._PlanSectionPageNumberPrefix] != null)
                                    htPN.Add(fileName, item[PlanSectionFieldId._PlanSectionPageNumberPrefix].ToString());

                                if (item[PlanSectionFieldId._PlanSectionAddPageNumbers] != null)
                                    htShowPn.Add(fileName, item[PlanSectionFieldId._PlanSectionAddPageNumbers].ToString() == "Yes" ? true : false);

                                if (item[PlanSectionFieldId._PlanSectionResetPageNumbers] != null)
                                    htRestart.Add(fileName, item[PlanSectionFieldId._PlanSectionResetPageNumbers].ToString() == "Yes" ? true : false);

                                if (item[PlanSectionFieldId._PlanSectionPageNumberStyle] != null)
                                    htType.Add(fileName, item[PlanSectionFieldId._PlanSectionPageNumberStyle].ToString());


                                //bb this was the old code to determine if the current document is a merged document or not and only merge the documents that
                                //are not already merged documents.  However, this logic was flawed because it always only checked for the current year so if a merged
                                //document was created a year or two ago then it would not be identified as a merged document.
                                //The new logic will look for the "IsMergedDocument" field.  If it finds it, it will use that field.  If it doesn't find it,
                                //it will use the section group "Merged Documents"
                                ////if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)


                                string isMergedDocument = "No";
                                if ((item.Fields.ContainsField("IsMergedDocument") && (item["IsMergedDocument"] != null)))
                                {

                                    isMergedDocument = item.GetFormattedValue("IsMergedDocument");
                                }
                                else
                                {
                                    //if we don't find the "IsMergedDocument" flag for some reason then use the section group of "Merged Documents" as the 
                                    //indicator as to whether a section is a merged document or not

                                    if (item["Plan Section Group"] != null)
                                    {
                                        string sectionGroup = (string)item["Plan Section Group"];

                                        if (sectionGroup.Equals("(00) Merged Documents"))
                                        {
                                            isMergedDocument = "Yes";
                                        }
                                    }

                                }

                                if (isMergedDocument == "No")
                                {

                                    FileStream stream = System.IO.File.Create(fileName);
                                    stream.Write(file.OpenBinary(), 0, file.OpenBinary().Length);
                                    stream.Close();
                                }
                            }

                            //merge

                            //DocBuilder db = new DocBuilder();

                            //string finalLocation = Folder + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".docx";
                            string finalLocation = list.Title + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".docx";
                            //BBTODO: why is the template type hardcoded to ESF here???  Also, location hardcoded.
                            InitializeDocument("C:\\merge\\" + finalLocation, TemplateTypes.esf, "C:\\merge\\");

                            string[] fileNames = Directory.GetFiles(dirName);

                            foreach (string fileName in fileNames)
                            {
                                string header1 = string.Empty;

                                //try
                                //{
                                //    header1 = htHdr[fileName].ToString();
                                //}
                                //catch
                                //{
                                //}
                                //string header2 = string.Empty;

                                //try
                                //{
                                //    header2 = ht[fileName].ToString();
                                //}
                                //catch
                                //{
                                //}
                                string pnp = string.Empty;
                                try
                                {
                                    pnp = htPN[fileName].ToString();
                                }
                                catch
                                {
                                }

                                bool addPageNumbers = false;
                                try
                                {
                                    addPageNumbers = Convert.ToBoolean(htShowPn[fileName]);
                                }
                                catch
                                {
                                }

                                bool resetPageNumbers = false;
                                try
                                {
                                    resetPageNumbers = Convert.ToBoolean(htRestart[fileName]);
                                }
                                catch
                                {
                                }

                                bool keepSourceFormatting = true;

                                keepSourceFormatting = KeepSourceFormatting;


                                try
                                {
                                    if (htType[fileName].ToString() == "arabic")
                                        AddDocument(fileName, "", "", "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.Arabic, keepSourceFormatting);
                                    else
                                        AddDocument(fileName, "", "", "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.RomanNumerials, keepSourceFormatting);
                                }
                                catch
                                {
                                    AddDocument(fileName, "", "", "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.Arabic, keepSourceFormatting);
                                }
                            }

                            FinalizeDocument();

                            web.AllowUnsafeUpdates = true;

                            String FolderParam = Folder;
                            int Index = FolderParam.LastIndexOf(@"/");
                            Folder = FolderParam.Substring(Index + 1);

                            SPFolder F = list.RootFolder.SubFolders[Folder];

                            FileStream docStream = File.OpenRead("C:\\merge\\" + finalLocation);

                            string libraryRelativePath = list.RootFolder.ServerRelativeUrl;
                            string libraryPath = siteCollection.MakeFullUrl(libraryRelativePath);
                            string documentPath = libraryPath + "/" + finalLocation;

                            Hashtable properties = new Hashtable();

                            properties["vti_title"] = finalLocation;

                            SPFile docfile = F.Files.Add(F.Name + "_" + DateTime.Now.ToLongDateString() + ".docx", docStream, properties, true);

                            SPListItem docitem = docfile.Item;

                            web.AllowUnsafeUpdates = true;
                            docitem["Plan Section Header Line 1"] = list.Title;
                            docitem["Plan Section Status"] = "Complete";
                            docitem["Plan Section Group"] = "(00) Merged Documents";
                            docitem["Plan Section NIMS Consistent"] = "N/A";
                            docitem["Plan Section Order"] = 0;

                            //If this item does not contain the "IsMergedDocument flag then add it.  This theoretically should never happen since we add this field
                            //to the list at the beginning of this function
                            if (!docitem.Fields.ContainsField("IsMergedDocument"))
                            {
                                docitem.Fields.Add("IsMergedDocument", SPFieldType.Choice, false);

                                SPField isMergeDocumentField = docitem.Fields.GetField("IsMergedDocument");
                                isMergeDocumentField.Type = SPFieldType.Choice;
                                isMergeDocumentField.Required = false;
                                isMergeDocumentField.Title = "IsMergedDocument";

                                docitem.SystemUpdate(false);
                            }

                            docitem["IsMergedDocument"] = "Yes";
                            docitem.SystemUpdate(false);

                            docStream.Close();

                            Directory.Delete(dirName, true);

                            web.AllowUnsafeUpdates = false;
                        }
                    });
                }
                catch (Exception ex)
                {
                    base.HandleException(ex, "MergeSectionUtil");
                }
                #endregion

            }
            else
            {
                #region EOPT Formatting

                string listId = ListId;

                try
                {

                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite site = new SPSite(SPControl.GetContextSite(HttpContext.Current).ID))
                        {
                            SPWeb web = SPControl.GetContextWeb(HttpContext.Current);
                            SPUser user = SPControl.GetContextWeb(HttpContext.Current).CurrentUser;
                            SPSite siteCollection = SPContext.Current.Site;

                            Guid listGuid = new Guid(listId);
                            SPList list = web.Lists[listGuid];

                            //linkListUrl.NavigateUrl = list.DefaultViewUrl;

                            Guid dirGuid = Guid.NewGuid();
                            string dirName = "c:\\temp\\eopt\\" + dirGuid.ToString() + "\\";
                            Directory.CreateDirectory(dirName);

                            Hashtable htHdr = new Hashtable();
                            Hashtable ht = new Hashtable();
                            Hashtable htPN = new Hashtable();
                            Hashtable htShowPn = new Hashtable();
                            Hashtable htRestart = new Hashtable();
                            Hashtable htType = new Hashtable();

                            //if this list does not contain the "IsMergedDocument" flag then we need to add it
                            if (!list.Fields.ContainsField("IsMergedDocument"))
                            {
                                web.AllowUnsafeUpdates = true;
                                list.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);
                                list.Update();
                                web.AllowUnsafeUpdates = false;
                            }


                            //if the "Section Group" drop down does not contain the value of "Merged Documents" in its selections
                            //then we need to add this option to the selection
                            if (list.Fields.ContainsField("Plan Section Section Group"))
                            {
                                SPFieldChoice ddlSectionGroup = (SPFieldChoice)list.Fields["Plan Section Section Group"];

                                if (!ddlSectionGroup.Choices.Contains("Merged Documents"))
                                {
                                    web.AllowUnsafeUpdates = true;
                                    ddlSectionGroup.Choices.Add("Merged Documents");
                                    ddlSectionGroup.Update();
                                    list.Update();
                                    web.AllowUnsafeUpdates = false;
                                }
                            }

                            //if the "Section Status" drop down does not contain the value of "Completed" in its selections
                            //then we need to add this option to the selection
                            if (list.Fields.ContainsField("Plan Section Section Status"))
                            {
                                SPFieldChoice ddlSectionStatus = (SPFieldChoice)list.Fields["Plan Section Section Status"];
                                if (!ddlSectionStatus.Choices.Contains("Complete"))
                                {
                                    web.AllowUnsafeUpdates = true;
                                    ddlSectionStatus.Choices.Add("Complete");
                                    ddlSectionStatus.Update();
                                    list.Update();
                                    web.AllowUnsafeUpdates = false;
                                }
                            }

                            SPListItemCollection Items = base.GetItemsMergeFields(list, Folder);

                            foreach (SPListItem item in Items)//list.Items)
                            {
                                SPFile file = item.File;

                                string section = "NoSection";
                                if (item["Plan Section Group"] != null)
                                {
                                    string temp = item["Plan Section Group"].ToString();

                                    temp = temp.Replace("(0", "");
                                    temp = temp.Replace(")", "");

                                    //string section = (string)item["Section Group"].ToString().Substring(1, 1).PadLeft(3, char.Parse("0"));
                                    section = temp.Substring(0, 1).PadLeft(3, char.Parse("0"));
                                }


                                string order = (string)item["Plan Section Order"].ToString().PadLeft(3, char.Parse("0"));
                                string fileName = dirName + section + order + "_" + file.Name;

                                if (item[PlanSectionFieldId._PlanSectionHeaderLine1] != null)
                                    htHdr.Add(fileName, item[PlanSectionFieldId._PlanSectionHeaderLine1].ToString());

                                if (item[PlanSectionFieldId._PlanSectionHeaderLine2] != null)
                                    ht.Add(fileName, item[PlanSectionFieldId._PlanSectionHeaderLine2].ToString());

                                if (item[PlanSectionFieldId._PlanSectionPageNumberPrefix] != null)
                                    htPN.Add(fileName, item[PlanSectionFieldId._PlanSectionPageNumberPrefix].ToString());

                                if (item[PlanSectionFieldId._PlanSectionAddPageNumbers] != null)
                                    htShowPn.Add(fileName, item[PlanSectionFieldId._PlanSectionAddPageNumbers].ToString() == "Yes" ? true : false);

                                if (item[PlanSectionFieldId._PlanSectionResetPageNumbers] != null)
                                    htRestart.Add(fileName, item[PlanSectionFieldId._PlanSectionResetPageNumbers].ToString() == "Yes" ? true : false);

                                if (item[PlanSectionFieldId._PlanSectionPageNumberStyle] != null)
                                    htType.Add(fileName, item[PlanSectionFieldId._PlanSectionPageNumberStyle].ToString());


                                //bb this was the old code to determine if the current document is a merged document or not and only merge the documents that
                                //are not already merged documents.  However, this logic was flawed because it always only checked for the current year so if a merged
                                //document was created a year or two ago then it would not be identified as a merged document.
                                //The new logic will look for the "IsMergedDocument" field.  If it finds it, it will use that field.  If it doesn't find it,
                                //it will use the section group "Merged Documents"
                                ////if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)


                                string isMergedDocument = "No";
                                if ((item.Fields.ContainsField("IsMergedDocument") && (item["IsMergedDocument"] != null)))
                                {

                                    isMergedDocument = item.GetFormattedValue("IsMergedDocument");
                                }
                                else
                                {
                                    //if we don't find the "IsMergedDocument" flag for some reason then use the section group of "Merged Documents" as the 
                                    //indicator as to whether a section is a merged document or not

                                    if (item["Plan Section Group"] != null)
                                    {
                                        string sectionGroup = (string)item["Plan Section Group"];

                                        if (sectionGroup.Equals("(00) Merged Documents"))
                                        {
                                            isMergedDocument = "Yes";
                                        }
                                    }

                                }
                                
                                if (isMergedDocument == "No")
                                {

                                    FileStream stream = System.IO.File.Create(fileName);
                                    stream.Write(file.OpenBinary(), 0, file.OpenBinary().Length);
                                    stream.Close();
                                }
                            }

                            //merge

                            //DocBuilder db = new DocBuilder();

                            //string finalLocation = Folder + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".docx";
                            string finalLocation = list.Title + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".docx";
                            //BBTODO: why is the template type hardcoded to ESF here???  Also, location hardcoded.
                            InitializeDocument("C:\\merge\\" + finalLocation, TemplateTypes.esf, "C:\\merge\\");

                            string[] fileNames = Directory.GetFiles(dirName);

                            foreach (string fileName in fileNames)
                            {
                                string header1 = string.Empty;

                                try
                                {
                                    header1 = htHdr[fileName].ToString();
                                }
                                catch
                                {
                                }
                                string header2 = string.Empty;

                                try
                                {
                                    header2 = ht[fileName].ToString();
                                }
                                catch
                                {
                                }
                                string pnp = string.Empty;
                                try
                                {
                                    pnp = htPN[fileName].ToString();
                                }
                                catch
                                {
                                }

                                bool addPageNumbers = false;
                                try
                                {
                                    addPageNumbers = Convert.ToBoolean(htShowPn[fileName]);
                                }
                                catch
                                {
                                }

                                bool resetPageNumbers = false;
                                try
                                {
                                    resetPageNumbers = Convert.ToBoolean(htRestart[fileName]);
                                }
                                catch
                                {
                                }

                                bool keepSourceFormatting = true;

                                keepSourceFormatting = KeepSourceFormatting;


                                try
                                {
                                    if (htType[fileName].ToString() == "arabic")
                                        AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.Arabic, keepSourceFormatting);
                                    else
                                        AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.RomanNumerials, keepSourceFormatting);
                                }
                                catch
                                {
                                    AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", PageNumberTypes.Arabic, keepSourceFormatting);
                                }
                            }

                            FinalizeDocument();

                            web.AllowUnsafeUpdates = true;

                            String FolderParam = Folder;
                            int Index = FolderParam.LastIndexOf(@"/");
                            Folder = FolderParam.Substring(Index + 1);

                            SPFolder F = list.RootFolder.SubFolders[Folder];

                            FileStream docStream = File.OpenRead("C:\\merge\\" + finalLocation);

                            string libraryRelativePath = list.RootFolder.ServerRelativeUrl;
                            string libraryPath = siteCollection.MakeFullUrl(libraryRelativePath);
                            string documentPath = libraryPath + "/" + finalLocation;

                            Hashtable properties = new Hashtable();

                            properties["vti_title"] = finalLocation;

                            SPFile docfile = F.Files.Add(F.Name + "_" + DateTime.Now.ToLongDateString() + ".docx", docStream, properties, true);                            

                            SPListItem docitem = docfile.Item;
                            
                            web.AllowUnsafeUpdates = true;
                            docitem["Plan Section Header Line 1"] = list.Title;
                            docitem["Plan Section Status"] = "Complete";
                            docitem["Plan Section Group"] = "(00) Merged Documents";
                            docitem["Plan Section NIMS Consistent"] = "N/A";
                            docitem["Plan Section Order"] = 0;

                            //If this item does not contain the "IsMergedDocument flag then add it.  This theoretically should never happen since we add this field
                            //to the list at the beginning of this function
                            if (!docitem.Fields.ContainsField("IsMergedDocument"))
                            {
                                docitem.Fields.Add("IsMergedDocument", SPFieldType.Choice, false);

                                SPField isMergeDocumentField = docitem.Fields.GetField("IsMergedDocument");
                                isMergeDocumentField.Type = SPFieldType.Choice;
                                isMergeDocumentField.Required = false;
                                isMergeDocumentField.Title = "IsMergedDocument";

                                docitem.SystemUpdate(false);
                            }

                            docitem["IsMergedDocument"] = "Yes";
                            docitem.SystemUpdate(false);

                            docStream.Close();

                            Directory.Delete(dirName, true);

                            web.AllowUnsafeUpdates = false;
                        }
                    });
                }
                catch (Exception ex)
                {
                    base.HandleException(ex, "MergeSectionUtil");
                }
                #endregion
            }
        }
        #endregion

        #endregion

    }

    #region Commented Code
    /* This is the Full Code Behind from MergeSections.aspx.cs
     * 
     * 
     * 
     */
    /*
    using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.DirectoryServices.AccountManagement;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using Microsoft.SharePoint.Administration;
using Aspose.Words;


public class DocBuilder
{
    public enum PageNumberTypes
    {
        RomanNumerials = 1,
        Arabic = 2
    }

    public enum TemplateTypes
    {
        basic_plan = 1,
        esf = 2,
        support_annex = 3
    }

    private Document mDocument;
    private string mDocumenNameandPath;
    private string mLocationOfLicenseAndTemplateFile;

    public void InitializeDocument(string FilePathandSpec, TemplateTypes TemplateType, string LocationOfLicenseAndFormattingFile)
    {
        if (String.IsNullOrEmpty(FilePathandSpec))
            throw new ArgumentException("FilePathandSpec is null or empty.", "FilePathandSpec");

        if (String.IsNullOrEmpty(LocationOfLicenseAndFormattingFile))
            throw new ArgumentException("LocationOfLicenseAndTemplateFile is null or empty.", "LocationOfLicenseAndTemplateFile");

        mDocumenNameandPath = FilePathandSpec;
        mLocationOfLicenseAndTemplateFile = LocationOfLicenseAndFormattingFile;
        FindAndApplyLicense();

        CreateDocument(TemplateType);
    }

    public void AddDocument(string DocumentNameandPath, string HeaderLine1, string HeaderLine2, string FooterText, Boolean AddPageNumbers,
        Boolean BeginNewPageNumbers, string PageNumberPreText, PageNumberTypes PageNumbering, bool keepSourceFormatting)
    {
        if (String.IsNullOrEmpty(DocumentNameandPath))
            throw new ArgumentException("DocumentNameandPath is null or empty.", "DocumentNameandPath");

        // Open the source document.
        Document srcDoc = new Document(DocumentNameandPath);
        DocumentBuilder builder = new DocumentBuilder(srcDoc);

        //if (FooterText.Length > 0)
        //{
        for (int sectIdx = 0; sectIdx < srcDoc.Sections.Count; sectIdx++)
        {
            //move documentbuilder cursor to the current section
            builder.MoveToSection(sectIdx);

            Section currentSection = builder.CurrentSection;
            PageSetup pageSetup = currentSection.PageSetup;
            currentSection.ClearHeadersFooters();

            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);

            builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
            // Set font properties for header text.
            builder.Font.Name = "Arial";
            builder.Font.Bold = true;
            builder.Font.Italic = true;
            builder.Font.Size = 9;

            if (HeaderLine1.Length > 0)
                builder.Write(HeaderLine1);

            if (HeaderLine2.Length > 0)
            {
                builder.Write("\n");
                builder.Write(HeaderLine2);
            }

            //move documentbuilder cursor to the footer
            builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary);

            builder.StartTable();

            // Calculate table width as total page width with left and right marins subtracted.
            double tableWidth = pageSetup.PageWidth - pageSetup.LeftMargin - pageSetup.RightMargin;

            builder.InsertCell();
            builder.CellFormat.Width = tableWidth * 2 / 3;

            builder.Font.Name = "Arial";
            builder.Font.Bold = true;
            builder.Font.Italic = true;
            builder.Font.Size = 9;

            //Insert text into teh footer
            builder.Write(FooterText);

            // Align this text to the left.
            builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Left;

            builder.InsertCell();
            builder.CellFormat.Width = tableWidth / 3;

            if (AddPageNumbers == true)
            {
                builder.Write(PageNumberPreText);
                builder.InsertField("PAGE", "");
                Section section = srcDoc.Sections[0];

                // Set first section page numbering.
                if (BeginNewPageNumbers == true)
                {
                    section.PageSetup.RestartPageNumbering = true;
                    section.PageSetup.PageStartingNumber = 1;

                }
                if (PageNumbering == PageNumberTypes.RomanNumerials)
                    section.PageSetup.PageNumberStyle = NumberStyle.LowercaseRoman;
                else
                    section.PageSetup.PageNumberStyle = NumberStyle.Arabic;
            }

            // Align this text to the right.
            builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Right;

            builder.EndRow();
            builder.EndTable();

        }
        //}

        if (keepSourceFormatting)
        {
            //mDocument.Document.AppendDocument(srcDoc, ImportFormatMode.KeepSourceFormatting);
	    mDocument.AppendDocument(srcDoc, ImportFormatMode.KeepSourceFormatting);
        }
        else
        {
            //mDocument.Document.AppendDocument(srcDoc, ImportFormatMode.UseDestinationStyles);
             mDocument.AppendDocument(srcDoc, ImportFormatMode.UseDestinationStyles);
        }
    }



    //public void AddDocument(string DocumentNameandPath, string FooterText)
    //{
    //    if (String.IsNullOrEmpty(DocumentNameandPath))
    //        throw new ArgumentException("DocumentNameandPath is null or empty.", "DocumentNameandPath");


    //    // Open the source document.
    //    Document srcDoc = new Document(DocumentNameandPath);

    //    if (FooterText.Length > 0)
    //    {

    //        foreach (Section srcSection in srcDoc)
    //        {
    //            HeaderFooter Footer = srcSection.HeadersFooters[HeaderFooterType.FooterPrimary];
    //            if (Footer == null)
    //            {
    //                Footer = new HeaderFooter(srcSection.Document, HeaderFooterType.FooterPrimary);
    //                srcSection.HeadersFooters.Add(Footer);
    //            }
    //        }
    //        srcDoc.FirstSection.HeadersFooters[HeaderFooterType.FooterPrimary].Range.Replace("*", FooterText, false, false);
    //    }


    //    mDocument.Document.AppendDocument(srcDoc, ImportFormatMode.UseDestinationStyles);

    //    //AppendDoc(mDocument, srcDoc, FooterText);

    //}

    public void FinalizeDocument()
    {
        try
        {
            mDocument.Save(mDocumenNameandPath);
        }
        catch
        {
           // MessageBox.Show("An Error occured while trying to save the merged document, Please make sure the document is not open and try again.");
        }
    }


    /// <summary>
    /// Search for Aspose.Words and Aspose.Pdf licenses in the application directory.
    /// The File.Exists check is only needed in this demo so it will work
    /// both when the license file is missing and when it is present.
    /// In your real application you just need to call SetLicense.
    /// </summary>
    private void FindAndApplyLicense()
    {
        // Try to find Aspose.Custom license.
        //string licenseFile = mLocationOfLicenseAndTemplateFile + "Aspose.Custom.lic";
        string licenseFile = mLocationOfLicenseAndTemplateFile + "Aspose.Words.lic";
        if (File.Exists(licenseFile))
        {
            LicenseAsposeWords(licenseFile);
            //LicenseAsposePdf(licenseFile);
            return;
        }
    }

    /// <summary>
    /// This code activates Aspose.Words license.
    /// If you don't specify a license, Aspose.Words will work in evaluation mode.
    /// </summary>
    private void LicenseAsposeWords(string licenseFile)
    {
        Aspose.Words.License licenseWords = new Aspose.Words.License();
        licenseWords.SetLicense(licenseFile);
    }

    /// <summary>
    /// This code activates Aspose.Pdf license.
    /// If you don't specify a license, Aspose.Pdf will work in evaluation mode.
    /// </summary>
    private void LicenseAsposePdf(string licenseFile)
    {
        //Aspose.Pdf.License licensePdf = new Aspose.Pdf.License();
        //licensePdf.SetLicense(licenseFile);
    }

    private void CreateDocument(TemplateTypes DocumentType)
    {
        try
        {

            string TemplateName = "";

            switch (DocumentType)
            {
                case TemplateTypes.basic_plan:
                    TemplateName = "basic_plan.doc";
                    break;
                case TemplateTypes.esf:
                    TemplateName = "esf.doc";
                    break;
                case TemplateTypes.support_annex:
                    TemplateName = "support_annex.doc";
               
                    break;
            }

            // Loads the document into Aspose.Words object model.
           
            mDocument = new Document(mLocationOfLicenseAndTemplateFile + TemplateName);
            mDocument.Sections.Clear();
        }
        catch (Exception ex)
        {
            throw ex;
            //Logger.LogMessageToFile(ex.InnerException.ToString());
        }


    }

    //private void AppendDoc(Document dstDoc, Document srcDoc, string FooterText)
    //{
    //    // Loop through all sections in the source document. 
    //    // Section nodes are immediate children of the Document node so we can just enumerate the Document.
    //    bool HeaderExist = false;

    //    foreach (Section srcSection in srcDoc)
    //    {
    //        HeaderFooter Footer = srcSection.HeadersFooters[HeaderFooterType.FooterPrimary];
    //        if (Footer == null)
    //        {
    //            Footer = new HeaderFooter(srcSection.Document, HeaderFooterType.FooterPrimary);
    //            srcSection.HeadersFooters.Add(Footer);
    //        }

    //        // Because we are copying a section from one document to another, 
    //        // it is required to import the Section node into the destination document.
    //        // This adjusts any document-specific references to styles, lists, etc.
    //        //
    //        // Importing a node creates a copy of the original node, but the copy
    //        // is ready to be inserted into the destination document.
    //        Node dstSection = dstDoc.ImportNode(srcSection, true, ImportFormatMode.UseDestinationStyles);

    //        foreach (Node node in srcSection)
    //        {
    //            // Every node has the NodeType property.
    //            switch (node.NodeType)
    //            {
    //                case NodeType.Body:
    //                    {
    //                        break;
    //                    }
    //                case NodeType.HeaderFooter:
    //                    {
    //                        // If the node type is HeaderFooter, we can cast the node to the HeaderFooter class.
    //                        HeaderFooter headerFooter = (HeaderFooter)node;

    //                        if (headerFooter.HeaderFooterType == HeaderFooterType.FooterPrimary)
    //                        {
    //                            HeaderExist = true;
    //                            if (FooterText.Length > 0)
    //                            {
    //                                headerFooter.Range.Replace("*", FooterText, false, false);
    //                                //dstDoc.FirstSection.HeadersFooters[HeaderFooterType.FooterPrimary].Range.Replace("*", FooterText, false, false);
    //                            }
    //                        }
    //                        break;
    //                    }
    //                default:
    //                    {
    //                        // Other types of nodes never occur inside a Section node.
    //                        throw new Exception("Unexpected node type in a section.");
    //                    }
    //            }
    //        }


    //        // Now the new section node can be appended to the destination document.
    //        dstDoc.AppendChild(dstSection);

    //        //DocumentBuilder builder = new DocumentBuilder(dstDoc);
    //        //builder.InsertBreak(BreakType.SectionBreakNewPage);

    //        //if (HeaderExist == false)
    //        //{
    //        //    builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary);
    //        //    builder.Write(FooterText);
    //        //}

    //    }
    //}

}

//Code used to merge the plan sections into one document
//how it's being done:
// Step 1: 
//    -  Take the ListId passed in the Request
//    -  generate a GUI and is it to create a temp directory in "c:\\temp\\eopt\\" + dirGuid.ToString() + "\\";
// Step 2:  for each list item ( i.e. plan section) do the following:
//    - get the Word file
//    - find out which section this file belongs to which section order it has
//    - then create a new filename for it using fileName = dirName + section + order + "_" + file.Name;
//    - gather the information about this file: what it has in the header, how many page numbers, page number style etc
//    - create a final location ( where the merged document will be saved
//    - get all the individual files ( plan sections)
//    -  for each file:
//       - retrieve the information about the header text, page numbers, page number style etc.
//       - add this document to the DocBuilder ( DocBuilder creates the merged version of these documents
// Step 3 : once the merged document is created, add it ( upload it) to Sharepoint and add a section at the bottom of the page that
//           has a link to that merged document.  


public partial class MergeSections : System.Web.UI.Page
{
    private static readonly string LOG_FILENAME = "C:\\Program Files\\Common Files\\Microsoft Shared\\web server extensions\\14\\DebugLogs\\DebugLog.txt";

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    string listId = Request["ListId"];

    //    try
    //    {

    //        SPSecurity.RunWithElevatedPrivileges(delegate()
    //        {
    //            using (SPSite site = new SPSite(SPControl.GetContextSite(HttpContext.Current).ID))
    //            {
    //                SPWeb web = SPControl.GetContextWeb(HttpContext.Current);
    //                SPUser user = SPControl.GetContextWeb(HttpContext.Current).CurrentUser;
    //                SPSite siteCollection = SPContext.Current.Site;

    //                Guid listGuid = new Guid(listId);
    //                SPList list = web.Lists[listGuid];

    //                linkListUrl.NavigateUrl = list.DefaultViewUrl;

    //                Guid dirGuid = Guid.NewGuid();
    //                string dirName = "c:\\temp\\eopt\\" + dirGuid.ToString() + "\\";
    //                Directory.CreateDirectory(dirName);

    //                Hashtable htHdr = new Hashtable();
    //                Hashtable ht = new Hashtable();
    //                Hashtable htPN = new Hashtable();
    //                Hashtable htShowPn = new Hashtable();
    //                Hashtable htRestart = new Hashtable();
    //                Hashtable htType = new Hashtable();

    //                //if this list does not contain the "IsMergedDocument" flag then we need to add it
    //                if (!list.Fields.ContainsField("IsMergedDocument"))
    //                {
    //                    web.AllowUnsafeUpdates = true;
    //                    list.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);
    //                    list.Update();
    //                    web.AllowUnsafeUpdates = false;
    //                }

    //                //if the "Section Group" drop down does not contain the value of "Merged Documents" in its selections
    //                //then we need to add this option to the selection
    //                if (list.Fields.ContainsField("Section Group"))
    //                {
    //                    SPFieldChoice ddlSectionGroup = (SPFieldChoice)list.Fields["Section Group"];

    //                    if (!ddlSectionGroup.Choices.Contains("Merged Documents"))
    //                    {
    //                        web.AllowUnsafeUpdates = true;
    //                        ddlSectionGroup.Choices.Add("Merged Documents");
    //                        ddlSectionGroup.Update();
    //                        list.Update();
    //                        web.AllowUnsafeUpdates = false;
    //                    }
    //                }

    //                //if the "Section Status" drop down does not contain the value of "Completed" in its selections
    //                //then we need to add this option to the selection
    //                if (list.Fields.ContainsField("Section Status"))
    //                {
    //                    SPFieldChoice ddlSectionStatus = (SPFieldChoice)list.Fields["Section Status"];
    //                    if (!ddlSectionStatus.Choices.Contains("Complete"))
    //                    {
    //                        web.AllowUnsafeUpdates = true;
    //                        ddlSectionStatus.Choices.Add("Complete");
    //                        ddlSectionStatus.Update();
    //                        list.Update();
    //                        web.AllowUnsafeUpdates = false;
    //                    }
    //                }

    //                foreach (SPListItem item in list.Items)
    //                {
    //                    SPFile file = item.File;

    //                    string section = "NoSection";
    //                    if (item["Section Group"] != null)
    //                    {
    //                        string temp = item["Section Group"].ToString();

    //                        temp = temp.Replace("(0", "");
    //                        temp = temp.Replace(")", "");

    //                        //string section = (string)item["Section Group"].ToString().Substring(1, 1).PadLeft(3, char.Parse("0"));
    //                        section = temp.Substring(0, 1).PadLeft(3, char.Parse("0"));
    //                    }

    //                    string order = (string)item["Section Order"].ToString().PadLeft(3, char.Parse("0"));
    //                    string fileName = dirName + section + order + "_" + file.Name;

    //                    if (item["Header Line 1"] != null)
    //                        htHdr.Add(fileName, item["Header Line 1"].ToString());

    //                    if (item["Header Line 2"] != null)
    //                        ht.Add(fileName, item["Header Line 2"].ToString());

    //                    if (item["Page Number Prefix"] != null)
    //                        htPN.Add(fileName, item["Page Number Prefix"].ToString());

    //                    if (item["Add Page Numbers"] != null)
    //                        htShowPn.Add(fileName, Convert.ToBoolean(item["Add Page Numbers"]));

    //                    if (item["Reset Page Numbers"] != null)
    //                        htRestart.Add(fileName, Convert.ToBoolean(item["Reset Page Numbers"]));

    //                    if (item["Page Number Style"] != null)
    //                        htType.Add(fileName, item["Page Number Style"].ToString());

    //                    //bb this was the old code to determine if the current document is a merged document or not and only merge the documents that
    //                    //are not already merged documents.  However, this logic was flawed because it always only checked for the current year so if a merged
    //                    //document was created a year or two ago then it would not be identified as a merged document.
    //                    //The new logic will look for the "IsMergedDocument" field.  If it finds it, it will use that field.  If it doesn't find it,
    //                    //it will use the section group "Merged Documents"
    //                    ////if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)
    //                    bool isMergedDocument = false;
    //                    if ((item.Fields.ContainsField("IsMergedDocument") && (item["IsMergedDocument"] != null)))
    //                    {
    //                        isMergedDocument = (bool)item["IsMergedDocument"];
    //                    }
    //                    else
    //                    {
    //                        //if we don't find the "IsMergedDocument" flag for some reason then use the section group of "Merged Documents" as the 
    //                        //indicator as to whether a section is a merged document or not
    //                        string sectionGroup = (string)item["Section Group"];
    //                        if (sectionGroup.Equals("Merged Documents"))
    //                        {
    //                            isMergedDocument = true;
    //                        }
    //                    }

    //                    //if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)
    //                    if (!isMergedDocument)
    //                    {

    //                        FileStream stream = System.IO.File.Create(fileName);
    //                        stream.Write(file.OpenBinary(), 0, file.OpenBinary().Length);
    //                        stream.Close();
    //                    }
    //                }

    //                //merge

    //                DocBuilder db = new DocBuilder();

    //                string finalLocation = list.Title + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".doc";

    //                //BBTODO: why is the template type hardcoded to ESF here???  Also, location hardcoded.
    //                db.InitializeDocument("C:\\merge\\" + finalLocation, DocBuilder.TemplateTypes.esf, "C:\\merge\\");

    //                string[] fileNames = Directory.GetFiles(dirName);

    //                foreach (string fileName in fileNames)
    //                {
    //                    string header1 = string.Empty;

    //                    try
    //                    {
    //                        header1 = htHdr[fileName].ToString();
    //                    }
    //                    catch
    //                    {
    //                    }
    //                    string header2 = string.Empty;

    //                    try
    //                    {
    //                        header2 = ht[fileName].ToString();
    //                    }
    //                    catch
    //                    {
    //                    }
    //                    string pnp = string.Empty;
    //                    try
    //                    {
    //                        pnp = htPN[fileName].ToString();
    //                    }
    //                    catch
    //                    {
    //                    }

    //                    bool addPageNumbers = false;
    //                    try
    //                    {
    //                        addPageNumbers = Convert.ToBoolean(htShowPn[fileName]);
    //                    }
    //                    catch
    //                    {
    //                    }

    //                    bool resetPageNumbers = false;
    //                    try
    //                    {
    //                        resetPageNumbers = Convert.ToBoolean(htRestart[fileName]);
    //                    }
    //                    catch
    //                    {
    //                    }


    //                    try
    //                    {
    //                        if (htType[fileName].ToString() == "arabic")
    //                            db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.Arabic);
    //                        else
    //                            db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.RomanNumerials);
    //                    }
    //                    catch
    //                    {
    //                        db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.Arabic);
    //                    }
    //                }

    //                db.FinalizeDocument();

    //                web.AllowUnsafeUpdates = true;

    //                FileStream docStream = File.OpenRead("C:\\merge\\" + finalLocation);

    //                string libraryRelativePath = list.RootFolder.ServerRelativeUrl;
    //                string libraryPath = siteCollection.MakeFullUrl(libraryRelativePath);
    //                string documentPath = libraryPath + "/" + finalLocation;

    //                Hashtable properties = new Hashtable();

    //                properties["vti_title"] = finalLocation;

    //                SPFile docfile = web.Files.Add(documentPath, docStream, properties, true);

    //                SPListItem docitem = docfile.Item;

    //                if (docitem.HasUniqueRoleAssignments == false)
    //                    docitem.BreakRoleInheritance(true);

    //                web.AllowUnsafeUpdates = true;
    //                docitem["Header Line 1"] = list.Title;
    //                docitem["Section Status"] = "Complete";
    //                docitem["Section Group"] = "Merged Documents";
    //                docitem["NIMS Consistent"] = "N/A";
    //                docitem["Section Order"] = 0;

    //                //If this item does not contain the "IsMergedDocument flag then add it.  This theoretically should never happen since we add this field
    //                //to the list at the beginning of this function
    //                if (!docitem.Fields.ContainsField("IsMergedDocument"))
    //                {
    //                    docitem.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);

    //                    SPField isMergeDocumentField = docitem.Fields.GetField("IsMergedDocument");
    //                    isMergeDocumentField.Type = SPFieldType.Boolean;
    //                    isMergeDocumentField.Required = false;
    //                    isMergeDocumentField.Title = "IsMergedDocument";

    //                    docitem.SystemUpdate(false);
    //                }


    //                docitem["IsMergedDocument"] = true;
    //                docitem.SystemUpdate(false);

    //                docStream.Close();

    //                Directory.Delete(dirName, true);

    //                web.AllowUnsafeUpdates = false;

    //                Response.Redirect(list.DefaultViewUrl);


    //            }
    //        });

    //    }
    //    catch (Exception ex)
    //    {
    //        if (!string.IsNullOrEmpty(ex.Message))
    //        {
    //            LogMessageToFile("Exception!!!  ex.Message is: " + ex.Message);
    //        }
    //        if (ex.InnerException != null)
    //        {
    //            LogMessageToFile("Exception!!! ex.InnerException is " + ex.InnerException.ToString());
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
       
        try
        {
            if (!Page.IsPostBack)
            {
                rdolstFormatting.Items[0].Selected = true;
            }

        }
        catch (Exception ex)
        {
            if (!string.IsNullOrEmpty(ex.Message))
            {
                LogMessageToFile("Exception on page load!!!  ex.Message is: " + ex.Message);
            }
            if (ex.InnerException != null)
            {
                LogMessageToFile("Exception on page load!!! ex.InnerException is " + ex.InnerException.ToString());
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnMerge_Click(object sender, EventArgs e)
    {
        MergePlanSections();
    }
    private void MergePlanSections()
    {
        panelInProgress.Visible = true;

        panelComplete.Visible = false;
        finalUrl.Visible = true;
         
        string listId = Request["ListId"];

        try
        {

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPControl.GetContextSite(HttpContext.Current).ID))
                {
                    SPWeb web = SPControl.GetContextWeb(HttpContext.Current);
                    SPUser user = SPControl.GetContextWeb(HttpContext.Current).CurrentUser;
                    SPSite siteCollection = SPContext.Current.Site;

                    Guid listGuid = new Guid(listId);
                    SPList list = web.Lists[listGuid];

                    linkListUrl.NavigateUrl = list.DefaultViewUrl;

                    Guid dirGuid = Guid.NewGuid();
                    string dirName = "c:\\temp\\eopt\\" + dirGuid.ToString() + "\\";
                    Directory.CreateDirectory(dirName);

                    Hashtable htHdr = new Hashtable();
                    Hashtable ht = new Hashtable();
                    Hashtable htPN = new Hashtable();
                    Hashtable htShowPn = new Hashtable();
                    Hashtable htRestart = new Hashtable();
                    Hashtable htType = new Hashtable();

                    //if this list does not contain the "IsMergedDocument" flag then we need to add it
                    if (!list.Fields.ContainsField("IsMergedDocument"))
                    {
                        web.AllowUnsafeUpdates = true;
                        list.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);
                        list.Update();
                        web.AllowUnsafeUpdates = false;
                    }


                    //if the "Section Group" drop down does not contain the value of "Merged Documents" in its selections
                    //then we need to add this option to the selection
                    if (list.Fields.ContainsField("Section Group"))
                    {
                        SPFieldChoice ddlSectionGroup = (SPFieldChoice)list.Fields["Section Group"];

                        if (!ddlSectionGroup.Choices.Contains("Merged Documents"))
                        {
                            web.AllowUnsafeUpdates = true;
                            ddlSectionGroup.Choices.Add("Merged Documents");
                            ddlSectionGroup.Update();
                            list.Update();
                            web.AllowUnsafeUpdates = false;
                        }
                    }

                    //if the "Section Status" drop down does not contain the value of "Completed" in its selections
                    //then we need to add this option to the selection
                    if (list.Fields.ContainsField("Section Status"))
                    {
                        SPFieldChoice ddlSectionStatus = (SPFieldChoice)list.Fields["Section Status"];
                        if (!ddlSectionStatus.Choices.Contains("Complete"))
                        {
                            web.AllowUnsafeUpdates = true;
                            ddlSectionStatus.Choices.Add("Complete");
                            ddlSectionStatus.Update();
                            list.Update();
                            web.AllowUnsafeUpdates = false;
                        }
                    }


                    foreach (SPListItem item in list.Items)
                    {
                        SPFile file = item.File;

                        string section = "NoSection";
                        if (item["Section Group"] != null)
                        {
                            string temp = item["Section Group"].ToString();

                            temp = temp.Replace("(0", "");
                            temp = temp.Replace(")", "");

                            //string section = (string)item["Section Group"].ToString().Substring(1, 1).PadLeft(3, char.Parse("0"));
                            section = temp.Substring(0, 1).PadLeft(3, char.Parse("0"));
                        }


                        string order = (string)item["Section Order"].ToString().PadLeft(3, char.Parse("0"));
                        string fileName = dirName + section + order + "_" + file.Name;

                        if (item["Header Line 1"] != null)
                            htHdr.Add(fileName, item["Header Line 1"].ToString());

                        if (item["Header Line 2"] != null)
                            ht.Add(fileName, item["Header Line 2"].ToString());

                        if (item["Page Number Prefix"] != null)
                            htPN.Add(fileName, item["Page Number Prefix"].ToString());

                        if (item["Add Page Numbers"] != null)
                            htShowPn.Add(fileName, Convert.ToBoolean(item["Add Page Numbers"]));

                        if (item["Reset Page Numbers"] != null)
                            htRestart.Add(fileName, Convert.ToBoolean(item["Reset Page Numbers"]));

                        if (item["Page Number Style"] != null)
                            htType.Add(fileName, item["Page Number Style"].ToString());


                        //bb this was the old code to determine if the current document is a merged document or not and only merge the documents that
                        //are not already merged documents.  However, this logic was flawed because it always only checked for the current year so if a merged
                        //document was created a year or two ago then it would not be identified as a merged document.
                        //The new logic will look for the "IsMergedDocument" field.  If it finds it, it will use that field.  If it doesn't find it,
                        //it will use the section group "Merged Documents"
                        ////if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)


                        bool isMergedDocument = false;
                        if ((item.Fields.ContainsField("IsMergedDocument") && (item["IsMergedDocument"] != null)))
                        {

                            isMergedDocument = (bool)item["IsMergedDocument"];
                        }
                        else
                        {
                            //if we don't find the "IsMergedDocument" flag for some reason then use the section group of "Merged Documents" as the 
                            //indicator as to whether a section is a merged document or not

 			    if (item["Section Group"] != null)
                            {
                                string sectionGroup = (string)item["Section Group"];

                                if (sectionGroup.Equals("Merged Documents"))
                                {
                                    isMergedDocument = true;
                                }
			    }

                        }


                        //if (fileName.IndexOf(DateTime.Today.Year.ToString()) < 0)
                        if (!isMergedDocument)
                        {

                            FileStream stream = System.IO.File.Create(fileName);
                            stream.Write(file.OpenBinary(), 0, file.OpenBinary().Length);
                            stream.Close();
                        }
                    }

                    //merge

                    DocBuilder db = new DocBuilder();

                    string finalLocation = list.Title + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Day.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Hour.ToString().PadLeft(2, char.Parse("0")) + DateTime.Now.Minute.ToString().PadLeft(2, char.Parse("0")) + ".doc";

                    //BBTODO: why is the template type hardcoded to ESF here???  Also, location hardcoded.
                    db.InitializeDocument("C:\\merge\\" + finalLocation, DocBuilder.TemplateTypes.esf, "C:\\merge\\");

                    string[] fileNames = Directory.GetFiles(dirName);

                    foreach (string fileName in fileNames)
                    {
                        string header1 = string.Empty;

                        try
                        {
                            header1 = htHdr[fileName].ToString();
                        }
                        catch
                        {
                        }
                        string header2 = string.Empty;

                        try
                        {
                            header2 = ht[fileName].ToString();
                        }
                        catch
                        {
                        }
                        string pnp = string.Empty;
                        try
                        {
                            pnp = htPN[fileName].ToString();
                        }
                        catch
                        {
                        }

                        bool addPageNumbers = false;
                        try
                        {
                            addPageNumbers = Convert.ToBoolean(htShowPn[fileName]);
                        }
                        catch
                        {
                        }

                        bool resetPageNumbers = false;
                        try
                        {
                            resetPageNumbers = Convert.ToBoolean(htRestart[fileName]);
                        }
                        catch
                        {
                        }

                        bool keepSourceFormatting = true;

                        keepSourceFormatting = rdolstFormatting.Items[0].Selected;
                       

                        try
                        {
                            if (htType[fileName].ToString() == "arabic")
                                db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.Arabic, keepSourceFormatting);
                            else
                                db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.RomanNumerials, keepSourceFormatting);
                        }
                        catch
                        {
                            db.AddDocument(fileName, header1, header2, "", addPageNumbers, resetPageNumbers, pnp + " ", DocBuilder.PageNumberTypes.Arabic, keepSourceFormatting);
                        }
                    }

                    db.FinalizeDocument();

                    web.AllowUnsafeUpdates = true;

                    FileStream docStream = File.OpenRead("C:\\merge\\" + finalLocation);

                    string libraryRelativePath = list.RootFolder.ServerRelativeUrl;
                    string libraryPath = siteCollection.MakeFullUrl(libraryRelativePath);
                    string documentPath = libraryPath + "/" + finalLocation;

                    Hashtable properties = new Hashtable();

                    properties["vti_title"] = finalLocation;

                    SPFile docfile = web.Files.Add(documentPath, docStream, properties, true);

                    SPListItem docitem = docfile.Item;

                    if (docitem.HasUniqueRoleAssignments == false)
                        docitem.BreakRoleInheritance(true);

                    web.AllowUnsafeUpdates = true;
                    docitem["Header Line 1"] = list.Title;
                    docitem["Section Status"] = "Complete";
                    docitem["Section Group"] = "Merged Documents";
                    docitem["NIMS Consistent"] = "N/A";
                    docitem["Section Order"] = 0;

                    //If this item does not contain the "IsMergedDocument flag then add it.  This theoretically should never happen since we add this field
                    //to the list at the beginning of this function
                    if (!docitem.Fields.ContainsField("IsMergedDocument"))
                    {
                        docitem.Fields.Add("IsMergedDocument", SPFieldType.Boolean, false);

                        SPField isMergeDocumentField = docitem.Fields.GetField("IsMergedDocument");
                        isMergeDocumentField.Type = SPFieldType.Boolean;
                        isMergeDocumentField.Required = false;
                        isMergeDocumentField.Title = "IsMergedDocument";

                        docitem.SystemUpdate(false);
                    }


                    docitem["IsMergedDocument"] = true;
                    docitem.SystemUpdate(false);

                    docStream.Close();

                    Directory.Delete(dirName, true);

                    web.AllowUnsafeUpdates = false;

                    panelInProgress.Visible = false;
                    panelComplete.Visible = true;
                    finalUrl.Visible = false;

                    //Response.Redirect(list.DefaultViewUrl);


                }
            });

        }
        catch (Exception ex)
        {
            panelInProgress.Visible = false;
            panelComplete.Visible = false;
            if (!string.IsNullOrEmpty(ex.Message))
            {
                LogMessageToFile("Exception!!!  ex.Message is: " + ex.Message);
            }
            if (ex.InnerException != null)
            {
                LogMessageToFile("Exception!!! ex.InnerException is " + ex.InnerException.ToString());
            }
        }
    
    }

    public static void LogMessageToFile(string msg)
    {

        msg = string.Format("{0:G}: {1}\r\n", DateTime.Now, msg);

        File.AppendAllText(LOG_FILENAME, msg);

    }
}


*/
    #endregion

}
