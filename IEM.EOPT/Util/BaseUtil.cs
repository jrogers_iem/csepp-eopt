﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.DocumentManagement.DocumentSets;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace IEM.EOPT.Util
{
    internal class BaseUtil
    {
        #region Internal Methods
        internal SPListItemCollection GetItems(SPList List, String Folder)
        {
            //Parse out the Folder param to get the Folder name - 
            String FolderParam = Folder;
            int Index = FolderParam.LastIndexOf(@"/");
            FolderParam = FolderParam.Substring(Index + 1);

            SPQuery Query = new SPQuery();
            //Define the Query...
            Query.Query = @"<OrderBy>
                                <FieldRef Name='PlanSectionOrder' Ascending='FALSE'></FieldRef>
                            </OrderBy>
                            <Where>
                             <Gt>
                                <FieldRef Name='ID'></FieldRef>
                                <Value Type='Number'>0</Value>
                             </Gt>
                           </Where>";
            //Set the ViewFields...
            Query.ViewFields = @"<FieldRef Name='Title' /><FieldRef Name='PlanSectionCategoryOrder' /><FieldRef Name='PlanSectionOrder' /><FieldRef Name='PlanSectionGroup' />";
            //Getting the folder object from the list 
            SPFolder ItemFolder = List.RootFolder.SubFolders[FolderParam];
            //Set the Folder property
            Query.Folder = ItemFolder;
            //Get the Items...
            SPListItemCollection Items = List.GetItems(Query);
            //Return to sender...
            return Items;
        }

        internal SPListItemCollection GetItemsMergeFields(SPList List, String Folder)
        {
            //Parse out the Folder param to get the Folder name - 
            String FolderParam = Folder;
            int Index = FolderParam.LastIndexOf(@"/");
            FolderParam = FolderParam.Substring(Index + 1);
            #region Commented Code
            //SPFolder folder = List.ParentWeb.GetFolder(FolderParam); 
            //Microsoft.Office.DocumentManagement.DocumentSets.DocumentSet docset = Microsoft.Office.DocumentManagement.DocumentSets.DocumentSet.GetDocumentSet(folder);
            //string url = docset.WelcomePageUrl;
            //SPFile defaultPage = List.ParentWeb.GetFile(url);
            //SPLimitedWebPartManager wpm = defaultPage.GetLimitedWebPartManager(System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared);
            //add webparts
            // wpm.AddWebPart(.....);
            //change the welcome page 
            //docset.ContentTypeTemplate.WelcomePageView = List.Views[0];
            //DocumentSetTemplate T =  docset.ContentTypeTemplate;

            //SPView View = T.WelcomePageView;//docset.ContentTypeTemplate.WelcomePageView;
            #endregion

            SPQuery Query = new SPQuery();
            //Define the Query...
            //Query.Query = View.Query;
            //Replaced...
            Query.Query = @"<OrderBy>
                                <FieldRef Name='PlanSectionOrder' Ascending='FALSE'></FieldRef>
                            </OrderBy>
                            <GroupBy Collapse ='FALSE' GroupLimit ='100'>
                                <FieldRef Name='PlanSectionGroup'></FieldRef>
                            </GroupBy>
                            <Where>
                             <Gt>
                                <FieldRef Name='ID'></FieldRef>
                                <Value Type='Number'>0</Value>
                             </Gt>
                           </Where>";
            //Set the ViewFields...
            Query.ViewFields = @"<FieldRef Name='Title' />
                                <FieldRef Name='PlanSectionCategoryOrder' />
                                <FieldRef Name='PlanSectionOrder' />
                                <FieldRef Name='PlanSectionGroup' />
                                <FieldRef Name='PlanSectionHeaderLine1' />
                                <FieldRef Name='PlanSectionHeaderLine2' />
                                <FieldRef Name='PlanSectionCategoryOrder' />
                                <FieldRef Name='PlanSectionAddPageNumbers' />
                                <FieldRef Name='PlanSectionCategoryOrder' />
                                <FieldRef Name='PlanSectionNIMSConsistent' />
                                <FieldRef Name='PlanSectionPageNumberPrefix' />
                                <FieldRef Name='PlanSectionPageNumberStyle' />
                                <FieldRef Name='PlanSectionResetPageNumbers' />
                                <FieldRef Name='IsMergedDocument' />";
            //Getting the folder object from the list 
            SPFolder ItemFolder = List.RootFolder.SubFolders[FolderParam];
            //Set the Folder property
            Query.Folder = ItemFolder; 
            //Get the Items...
            SPListItemCollection Items = List.GetItems(Query);
            //Return to sender...
            return Items;
        }
        
        internal void PopulateDropDownList(SPList List, System.Web.UI.WebControls.DropDownList DropDownList1,  String ChoiceField, String Selected)
        {
            //Get the Field...
            SPFieldChoice GroupChoices = (SPFieldChoice)List.Fields.GetFieldByInternalName(ChoiceField);
            //Clear the list, just to be sure...
            DropDownList1.Items.Clear();
            //get the list of all possible Section Groups
            foreach (string Choice in GroupChoices.Choices)
            {
                //Define the Item...
                System.Web.UI.WebControls.ListItem Item = new System.Web.UI.WebControls.ListItem(Choice, Choice);
                //Make sure it's not already in there...
                if (!DropDownList1.Items.Contains(Item))
                {
                    //Add it...
                    DropDownList1.Items.Add(Item);
                    //If it's the selected one, set it...
                    if (Item.Text == Selected) { Item.Selected = true; }
                }
            }            
        }
        internal int GetPlanSectionCategoryOrder(string PlanSectionGroup)
        {
            //Define a local string...
            string Order = PlanSectionGroup;
            //Remove the first paren...
            Order = Order.Replace("(", "");
            //Get the last paren...
            int Index = Order.LastIndexOf(")");
            //Strip off the last part...
            Order = Order.Remove(Index, (Order.Length - 2));
            //Remove the leading zeros...
            Order = Order.TrimStart(new char[] { '0' });
            //We'll say it's 0 for now...
            int k = 0;
            //Parse it out...
            int.TryParse(Order, out k);
            //return to sender...
            return k;
        }
        #endregion

        #region Exception Handler
        internal void HandleException(Exception ex, String source)
        {
            //Write this to the ULS....
            SPDiagnosticsService Service = SPDiagnosticsService.Local;
            Service.WriteTrace(
                0, 
                new SPDiagnosticsCategory("IEM.EOPT", TraceSeverity.High, EventSeverity.Error), // create a category                                
                TraceSeverity.High, // set the logging level of this record                
                ex.Message, // message                
                source // parameters to message
                );
        }
        #endregion
    }

    internal sealed class PlanSectionFieldId
    {
        #region Fields
        public static string __ModerationComments = "_ModerationComments";
        public static string _FileLeafRef = "FileLeafRef";
        public static string _Title = "Title";
        public static string _ParentLeafName = "ParentLeafName";
        public static string _ParentVersionString = "ParentVersionString";
        public static string _DocumentSetDescription = "DocumentSetDescription";
        public static string _Jurisdiction = "Jurisdiction";
        public static string _Plan_x0020_Managers = "Plan_x0020_Managers";
        public static string _Plan_x0020_Writers = "Plan_x0020_Writers";
        public static string _Plan_x0020_Viewers = "Plan_x0020_Viewers";
        public static string _Plan_x0020_Owner = "Plan_x0020_Owner";
        public static string _Plan_x0020_Owner_x0020_Email = "Plan_x0020_Owner_x0020_Email";
        public static string _Plan_x0020_Owner_x0020_Phone = "Plan_x0020_Owner_x0020_Phone";
        public static string _PlanSectionGroup = "PlanSectionGroup";
        public static string _PlanSectionOrder = "PlanSectionOrder";
        public static string _PlanSectionStatus = "PlanSectionStatus";
        public static string _PlanSectionNIMSConsistent = "PlanSectionNIMSConsistent";
        public static string _PlanSectionAddPageNumbers = "PlanSectionAddPageNumbers";
        public static string _PlanSectionApproverComments = "PlanSectionApproverComments";
        public static string _PlanSectionDocId = "PlanSectionDocId";
        public static string _PlanSectionFooterText = "PlanSectionFooterText";
        public static string _PlanSectionHeaderLine1 = "PlanSectionHeaderLine1";
        public static string _PlanSectionHeaderLine2 = "PlanSectionHeaderLine2";
        public static string _PlanSectionNIMSCoach = "PlanSectionNIMSCoach";
        public static string _PlanSectionPageNumberPrefix = "PlanSectionPageNumberPrefix";
        public static string _PlanSectionPageNumberStyle = "PlanSectionPageNumberStyle";
        public static string _PlanSectionPlanningCoach = "PlanSectionPlanningCoach";
        public static string _PlanSectionSampleText = "PlanSectionSampleText";
        public static string _PlanSectionResetPageNumbers = "PlanSectionResetPageNumbers";
        public static string _PlanSectionCategoryOrder = "PlanSectionCategoryOrder";
        public static string _PlanSectionREPPCoach = "PlanSectionREPPCoach";
        public static string _SectionA = "SectionA";
        public static string __ModerationStatus = "_ModerationStatus";
        public static string _IsMergedDocument = "IsMergedDocument";
        public static string _ID = "ID";
        public static string _ContentType = "ContentType";
        public static string _Created = "Created";
        public static string _Author = "Author";
        public static string _Modified = "Modified";
        public static string _Editor = "Editor";
        public static string __CopySource = "_CopySource";
        public static string _CheckoutUser = "CheckoutUser";
        public static string __CheckinComment = "_CheckinComment";
        public static string _LinkFilenameNoMenu = "LinkFilenameNoMenu";
        public static string _LinkFilename = "LinkFilename";
        public static string _DocIcon = "DocIcon";
        public static string _FileSizeDisplay = "FileSizeDisplay";
        public static string _ItemChildCount = "ItemChildCount";
        public static string _FolderChildCount = "FolderChildCount";
        public static string _AppAuthor = "AppAuthor";
        public static string _AppEditor = "AppEditor";
        public static string _Edit = "Edit";
        public static string __UIVersionString = "_UIVersionString";

        #endregion
    }
}
