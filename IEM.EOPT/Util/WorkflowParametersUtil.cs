﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Serialization;

namespace IEM.EOPT.Util
{    

    [Serializable()]
    public class WorkflowParametersUtil
    {
        #region Properties
        private int _NumberOfApprovers = default(int);
        
        public int NumberOfApprovers
        {
            get { return _NumberOfApprovers; }
            set { _NumberOfApprovers = value; }
        }
        #endregion

        #region Get XML
        public string getInitXmlString(WorkflowParametersUtil Params)
        {
            //Decalse the data...
            WorkflowParametersUtil Data = new WorkflowParametersUtil();
            //Set the Params...
            Data._NumberOfApprovers = Params._NumberOfApprovers;
            //Now write the values to Memory...
            using (MemoryStream stream = new MemoryStream())
            {
                //Define the serializer...
                XmlSerializer serializer = new XmlSerializer(typeof(WorkflowParametersUtil));
                //Add the Data to the Stream...
                serializer.Serialize(stream, Data);
                //Reset...
                stream.Position = 0;
                //Define the array...
                byte[] bytes = new byte[stream.Length];
                //Read it in...
                stream.Read(bytes, 0, bytes.Length);
                //Be sure to encode this or the workflow won't understand it...
                return Encoding.UTF8.GetString(bytes);
            }
        }
        #endregion
    }
}
