﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class ToggleNIMS : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ManageSectionUtil Util = new ManageSectionUtil();

            Util.ToggleNIMSStatus(Request.Params["ListId"], Request.Params["ItemId"]);

            Context.Response.Write("<script type='text/javascript'>window.frameElement.commitPopup();</script>");
            Context.Response.Flush();
            Context.Response.End();
        }
    }
}
