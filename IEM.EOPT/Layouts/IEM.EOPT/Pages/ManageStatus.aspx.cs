﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class ManageStatus : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   //Our Util class...
                ManageSectionUtil Util = new ManageSectionUtil();
                //Set the Status based on the querystring param S...
                Util.SetSectionStatus(Request.Params[_ListId], Request.Params[_ItemId], Request.Params["S"]);
                //Close the dialog...
                CloseDialog();
            }
            catch (Exception ex)
            {
                //Log it to the ULS...
                HandleException(ex, this.GetType().FullName + ".PageLoad");
                //Show the user...
                ShowException(ex);
            }
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion
    }
}
