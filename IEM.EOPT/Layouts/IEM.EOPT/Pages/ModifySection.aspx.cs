﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using System.Collections;
using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class ModifySection : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                BindForm();
        }
        #endregion

        #region Private Methods
        private void BindForm()
        {
            try
            {
                using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
                {   //Get the Web...
                    using (SPWeb Web = Site.OpenWeb())
                    {   //Get the List...
                        SPList List = Web.Lists[new Guid(Request.Params[_ListId])];
                        //Get the Item...
                        SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(Request.Params[_SelectedItemId]));
                        //Set the Title...
                        TextBox1.Text = Item.GetFormattedValue(PlanSectionFieldId._Title);
                        //Our Util class...
                        ManageSectionUtil Util = new ManageSectionUtil();
                        //Populate the DropDown...
                        Util.PopulateDropDownList(GetList(), DropDownList1, PlanSectionFieldId._PlanSectionGroup, Item.GetFormattedValue(PlanSectionFieldId._PlanSectionGroup));
                        //Set the default value...
                        TextBox2.Text = Item.GetFormattedValue(PlanSectionFieldId._PlanSectionOrder);
                        //Get the NIMS Compliant...
                        String NIMS = Item.GetFormattedValue(PlanSectionFieldId._PlanSectionNIMSConsistent);
                        //Set the NIMS Complaint...
                        switch(NIMS){

                            case "Yes": RadioButtonList2.SelectedIndex = 0; break;
                            case "No":  RadioButtonList2.SelectedIndex = 1; break;
                            case "N/A": RadioButtonList2.SelectedIndex = 2; break;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                //Lot it to the ULS...
                HandleException(ex, "ModifySection.BindForm");
                //Display it to the User...
                ShowException(ex);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Updates the Plan Section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {   //Get the Site...
            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {   //Get the Web...
                using (SPWeb Web = Site.OpenWeb())
                {   //Our Util class...
                    ManageSectionUtil Util = new ManageSectionUtil();
                    //Update the Plan Section....
                    //switch (RadioButtonList1.SelectedIndex)
                    //{
                    //    case 0:
                    //        Util.UpdateSection(Web, Request[_ListId], Request[_SelectedItemId], TextBox1.Text, DropDownList1.SelectedValue, Int32.Parse(TextBox2.Text));
                    //        break;
                    //    case 1:
                    //        Util.UpdateSection(Web, Request[_ListId], Request[_SelectedItemId], TextBox1.Text, TextBox3.Text, 1);
                    //        break;
                    //}
                    
                }
            }                    
            //Close the Dialog to Refresh the page...
            CloseDialog();
        }
        /// <summary>
        /// Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {   //Close the Dialog to Refresh the page...
            CloseDialog();
        }
        /// <summary>
        /// Resets the Next Section Order for the Plan Section Group.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {   //Our Util class...
            ManageSectionUtil Util = new ManageSectionUtil();
            //Get the Next Section Order for the Group...
            int k = Util.GetNextSectionOrder(Request.Params[_SiteUrl], Request.Params[_ListId], Request.Params[_Folder], DropDownList1.SelectedItem.Text);
            //Set it...
            TextBox2.Text = k.ToString();
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //switch (RadioButtonList1.SelectedIndex)
            //{
            //    case 0:
            //        TR01.Visible = true;
            //        TR02.Visible = false;
            //        DropDownList1_SelectedIndexChanged(null, null);
            //        break;

            //    case 1:
            //        TR01.Visible = false;
            //        TR02.Visible = true;
            //        TextBox2.Text = "1";
            //        break;
            //}
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion

        
    }
}
