﻿using System;
using System.Collections;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class RequestApproval : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                BindForm();
        }
        #endregion

        #region Private Methods
        private void BindForm()
        {   //Get the Site...
            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {   //Get the Web...
                using (SPWeb Web = Site.OpenWeb())
                {   //Get the List...
                    SPList List = Web.Lists[new Guid(Request[_ListId])];
                    //Get the Item...
                    SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(Request.Params[_SelectedItemId]));
                    //Set the Title...
                    Label1.Text = Item[PlanSectionFieldId._Title].ToString();
                    //Get the Plan Managers field...
                    SPFieldUser UserField = (SPFieldUser)Item.Fields.GetField(PlanSectionFieldId._Plan_x0020_Managers);
                    //Get the contents...
                    String PlanManagers = Item.GetFormattedValue(PlanSectionFieldId._Plan_x0020_Managers);
                    //This is for users who have no email address...
                    ArrayList InvalidEmailList = new ArrayList();
                    //Make sure something came back...
                    if (!String.IsNullOrEmpty(PlanManagers))
                    {
                        SPFieldUserValueCollection UserFieldValueCollection = (SPFieldUserValueCollection)UserField.GetFieldValue(Item[PlanSectionFieldId._Plan_x0020_Managers].ToString());
                        //Now loop through and populate the ListBox...
                        foreach (SPFieldUserValue UserFieldValue in UserFieldValueCollection)
                        {
                            ListBox1.Items.Add(UserFieldValue.User.Name + " - " + UserFieldValue.User.Email);
                            //Check the email, if it's missing, add it to the list...
                            if (String.IsNullOrEmpty(UserFieldValue.User.Email)) { InvalidEmailList.Add(UserFieldValue.User.Name + " has no email address."); }
                        }                        
                    }
                    else
                    {
                        //Turn off the Start button if there are no Plan Managers...
                        Button1.Enabled = false;
                    }

                    if (InvalidEmailList.Count >= 1)
                    {
                        System.Text.StringBuilder Builder = new System.Text.StringBuilder("<br />These Plan Managers have no email address defined. Workflow tasks will be created, but no email notification can be sent. <br /><ul>");
                        //Build the unordered list...
                        for(int i = 0; i < InvalidEmailList.Count; i++){
                            Builder.AppendFormat("<li>{0}</li>", InvalidEmailList[i].ToString());
                        }
                        //Don't forget to cap the end...
                        Builder.Append("</ul>");
                        //Set the HTML...
                        WarningDiv.InnerHtml = Builder.ToString();
                        //Show the warning...
                        WarningDiv.Visible = true;
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Starts the Workflow on the Selected Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //The Return URL....
                string ReturnURL = this.Page.Request.Url.PathAndQuery;
                //This will close the modal dialog...
                string EndScript = "try{window.frameElement.commonModalDialogClose(0, 0);}catch (error){window.location.href = '" + ReturnURL + "';}";
                //Define the long operation...
                using (SPLongOperation OP = new SPLongOperation(this.Page))
                {   //Set some text for the user to read...
                    OP.LeadingHTML = "Starting Plan Section Approval, please wait...";
                    OP.TrailingHTML = "Please do not close this window during this process...";
                    //This kicks it off...
                    OP.Begin();
                    //Our Util class...
                    RequestApprovalUtil Util = new RequestApprovalUtil();                    
                    //Kick off the Workflow...
                    Util.StartWorkflow(Request.Params[_SiteUrl], Request[_ListId], Request[_SelectedItemId], ListBox1.Items.Count, "Section Approval");    

                    OP.EndScript(EndScript);
                }
                                
            }
            catch (Exception ex) { Label1.Text = ex.StackTrace; }
        }
        /// <summary>
        /// Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }
        #endregion
    }
}
