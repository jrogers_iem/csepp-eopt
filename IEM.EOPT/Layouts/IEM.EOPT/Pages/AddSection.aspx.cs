﻿using System;
using System.Collections;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class AddSection : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            //Hide this so it's not always present...
            ErrorDiv.Visible = false;

            if (!Page.IsPostBack)
                BindForm();
        }
        #endregion

        #region Bind Form
        /// <summary>
        /// Binds the page to the 
        /// </summary>
        private void BindForm()
        {
            //Get the Site...
            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {   //Get the Web...
                using (SPWeb Web = Site.OpenWeb())
                {   //Get the List...
                    SPList List = GetList();// Web.Lists[new Guid(Request.Params[_ListId])];

                    #region Group

                    ManageSectionUtil Util = new ManageSectionUtil();
                    Util.PopulateDropDownList(List, DropDownList1, PlanSectionFieldId._PlanSectionGroup, "");

                    #endregion

                    #region Order
                    //Parse out the Folder param to get the Folder name - 
                    String FolderParam = Request.Params[_Folder];
                    int Index = FolderParam.LastIndexOf(@"/");
                    FolderParam = FolderParam.Substring(Index+1);//, (FolderParam.Length-1));

                    //Getting the folder object from the list 
                    SPFolder Folder = List.RootFolder.SubFolders[FolderParam];
                    //Set the default value...
                    TextBox2.Text = Util.GetNextSectionOrder(Request.Params[_SiteUrl], Request.Params[_ListId], FolderParam, DropDownList1.SelectedItem.Text).ToString();
                    #endregion

                    #region Set Hidden Fields
                    HiddenField1.Value = Folder.UniqueId.ToString();
                    #endregion
                }
            }
            
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Adds the section to the document set.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {   //Get the Site...
                using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
                {   //Get the Web...
                    using (SPWeb Web = Site.OpenWeb())
                    {   //Our Util class...
                        ManageSectionUtil Util = new ManageSectionUtil();
                        //Make sure it has a file...
                        if (FileUpload1.HasFile) //This is validated client-side, but check just to be sure...
                        {   //Validate the file name....
                            if (Microsoft.SharePoint.Utilities.SPUrlUtility.IsLegalFileName(FileUpload1.FileName))
                            {   //If it all checks out, add the file...
                                Util.AddSection(Web, new Guid(HiddenField1.Value), TextBox1.Text, DropDownList1.SelectedValue, Int32.Parse(TextBox2.Text), FileUpload1.FileName, FileUpload1.FileBytes);
                            }
                            else
                            {   //Let the user know there was something wrong...
                                throw new Exception(FileUpload1.FileName + " is not valid. Please rename this file.");
                            }
                        }
                    }
                }
                //Close the Dialog to Refresh the page...
                CloseDialog();
            }
            catch (Exception ex)
            {
                //Pass it to the base class for logging...
                HandleException(ex, "AddSection.Button1_Click");
                //Show the user...
                ShowException(ex);
            }
        }
        /// <summary>
        /// Closes the modal dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }
        /// <summary>
        /// Sets TextBox2 to the next avaialble section order.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Our Util class...
                ManageSectionUtil Util = new ManageSectionUtil();
                //Get the next value...
                int k = Util.GetNextSectionOrder(Request.Params[_SiteUrl], Request.Params[_ListId], Request.Params[_Folder], DropDownList1.SelectedItem.Text);
                //Set the field...
                TextBox2.Text = k.ToString();
            }
            catch (Exception ex)
            {
                //Pass it to the base class for logging...
                HandleException(ex, "AddSection.DropDownList_SelectedIndexChanged");
                //Show the user...
                ShowException(ex);
            }
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion
    }
}
