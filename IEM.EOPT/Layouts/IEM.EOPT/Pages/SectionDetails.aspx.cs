﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using System.Collections;
using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class SectionDetails : EOPTBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                BindForm();
        }

        private void BindForm()
        {

            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {
                using (SPWeb Web = Site.OpenWeb())
                {
                    SPList List = Web.Lists[new Guid(Request.Params[_ListId])];

                    SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(Request.Params[_SelectedItemId]));

                    #region Title
                    TextBox1.Text = Item.GetFormattedValue(PlanSectionFieldId._Title);
                    #endregion

                    
                    

                    DropDownList1.Items.Add(Item.GetFormattedValue(PlanSectionFieldId._PlanSectionGroup));
                    

                     
                    //Set the default value...
                    TextBox2.Text = Item.GetFormattedValue(PlanSectionFieldId._PlanSectionOrder);
                     
                }
            }

        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SPSite Site = new SPSite(Request.Params["SiteUrl"]))
            {
                using (SPWeb Web = Site.OpenWeb())
                {
                    ManageSectionUtil Util = new ManageSectionUtil();

                    //Util.UpdateSection(Web, Request["ListId"], Request["SelectedItemId"], DropDownList1.SelectedValue, Int32.Parse(TextBox2.Text));
                }
            }
            //Close the Dialog to Refresh the page...
            CloseDialog();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }
    }
}
