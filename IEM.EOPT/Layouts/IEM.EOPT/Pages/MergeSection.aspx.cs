﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class MergeSection : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion        

        #region Event Handlers
        /// <summary>
        /// Merge section button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {   //The ReturnURL...
                string ReturnURL = this.Page.Request.Url.PathAndQuery;
                //This ill close the dialog window when complete...
                string EndScript = "try{window.frameElement.commonModalDialogClose(0, 0);}catch (error){window.location.href = '" + ReturnURL + "';}";
                //Define the Long Op....
                using (SPLongOperation OP = new SPLongOperation(this.Page))
                {   //Set some text for the user to read...
                    OP.LeadingHTML = "Merging Sections, please wait...";
                    OP.TrailingHTML = "Please do not close this window during the build...";
                    OP.Begin();
                    //Our Util class..
                    MergeSectionUtil Util = new MergeSectionUtil();
                    //Merge the sections...
                    Util.MergePlanSections(Request.Params[_ListId], Request.Params[_Folder], bool.Parse(RadioButtonList1.SelectedValue));
                    //Run the end script to force the close...
                    OP.EndScript(EndScript);
                }
            }            
            catch (Exception ex) 
            {
                //Log it to the ULS...
                HandleException(ex, "MergeSections.Button1_Click");
                //Display it to the User...
                ShowException(ex);
            }
        }
        /// <summary>
        /// Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion
    }
}
