﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using System.IO;
using Microsoft.Office.Word.Server.Conversions;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class ConvertToPDF : EOPTBasePage
    {
        static string WORD_AUTOMATION_SERVICE = "Word Automation Services";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPList List = SPContext.Current.Web.Lists[new Guid(Request.Params["ListId"])];
                SPListItem Item = List.GetItemById(Int32.Parse(Request.Params["ItemId"]));
                Label1.Text = Item[SPBuiltInFieldId.Title] as String;

                String IsMerged = Item["IsMergedDocument"] as String;

                if (String.IsNullOrEmpty(IsMerged) || IsMerged == "No")
                {
                    ShowException(new Exception("Only Merged Documents can be converted."));
                    Button1.Enabled = false;
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
             try
            {   //The ReturnURL...
                string ReturnURL = this.Page.Request.Url.PathAndQuery;
                //This ill close the dialog window when complete...
                string EndScript = "try{window.frameElement.commonModalDialogClose(0, 0);}catch (error){window.location.href = '" + ReturnURL + "';}";
                //Define the Long Op....
                using (SPLongOperation OP = new SPLongOperation(this.Page))
                {   //Set some text for the user to read...
                    OP.LeadingHTML = "Converting to PDF, please wait...";
                    OP.TrailingHTML = "Please do not close this window during the build...";
                    OP.Begin();
                    //Get current site context         
                    SPList List = SPContext.Current.Web.Lists[new Guid(Request.Params["ListId"])];
                    SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(Request.Params["ItemId"]));

                    if (Item != null)
                    {
                        WordDocsToConvertToPdf(Item);
                    }
            
                    //Run the end script to force the close...
                    OP.EndScript(EndScript);
                }
            }
             catch (Exception ex)
             {
                 //Log it to the ULS...
                 HandleException(ex, "ConvertToPDF.Button1_Click");
                 //Display it to the User...
                 ShowException(ex);
             }

        }

        private void WordDocsToConvertToPdf(SPListItem Item)
        {

            //Perform the conversion in memory first, therefore we require a MemoryStream.
            using (MemoryStream MS = new MemoryStream())
            {
                //Call the syncConverter class, passing in the name of the Word Automation Service for your Farm.
                SyncConverter SC = new SyncConverter(WORD_AUTOMATION_SERVICE);
                //Pass in your User Token or credentials under which this conversion job is executed.
                SC.UserToken = SPContext.Current.Site.UserToken;
                SC.Settings.UpdateFields = true;

                //Save format
                SC.Settings.OutputFormat = SaveFormat.PDF;

                //Convert to PDF by opening the file stream, and then converting to the destination memory stream.
                ConversionItemInfo Info = SC.Convert(Item.File.OpenBinaryStream(), MS);

                var Filename = Path.GetFileNameWithoutExtension(Item.File.Name) + ".pdf";
                if (Info.Succeeded)
                {
                    //File conversion successful, then add the memory stream to the SharePoint list.
                    SPFile File = Item.File.ParentFolder.Files.Add(Filename, MS, true);
                    //Ok, now set the Item's properties
                    File.Item["Plan Section Status"] = "Complete";
                    File.Item["Plan Section Group"] = "(00) Merged Documents";
                    File.Item["Plan Section NIMS Consistent"] = "N/A";
                    File.Item["Plan Section Order"] = 0;
                    File.Item["IsMergedDocument"] = "Yes";
                    File.Item.SystemUpdate(false);
                }
                else if (Info.Failed)
                {
                    throw new Exception(Info.ErrorMessage);
                }
            }
        }

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion

    }
}
