﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Layouts;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class RemoveSection : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
                {   //Get the Web...
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        //Set the flag to be avle to do this on a GET request...
                        Web.AllowUnsafeUpdates = true;
                        //Get the List...
                        SPList List = Web.Lists[new Guid(Request.Params[_ListId])];
                        //Return the Item...
                        SPListItem Item = List.GetItemById(Int32.Parse(Request.Params[_SelectedItemId]));
                        //Send it to the Recycle Bin...
                        Item.Recycle();
                        //Put the flag back....
                        Web.AllowUnsafeUpdates = false;
                    }
                }
                //Close the dialog...
                CloseDialog();
            }
            catch (Exception ex)
            {
                //Log it to the ULS...
                HandleException(ex, "RemoveSection.PageLoad");
                //Show the user...
                ShowException(ex);
            }
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion
    }
}
