﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestApproval.aspx.cs" Inherits="IEM.EOPT.Layouts.IEM.EOPT.Pages.RequestApproval" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    
    <div id="ErrorDiv" class="Error" runat="server" visible="false"></div>  
    <table style="width: 100%;">
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>You are starting the approval workflow for section
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>.</p>
                <br />
                <p>Each Plan Owner will be assigned a Workflow Task. They will have the option to Approve/Reject the Plan Section.</p>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr valign="top">
            <td>Plan Owners:</td>
            <td>
                <asp:ListBox ID="ListBox1" runat="server" Width="100%"></asp:ListBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="WarningDiv" runat="server" visible="false" class="Error">                    
                </div>
                <br />
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right">
                <asp:Button ID="Button1" runat="server" Text="Start" OnClick="Button1_Click" />&nbsp;&nbsp;
                        <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="Button2_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>       
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Request Approval
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Request Approval
</asp:Content>
