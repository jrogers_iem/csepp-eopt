﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public partial class MoveSection : EOPTBasePage
    {
        #region Page Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ManageSectionUtil Util = new ManageSectionUtil();

                if (Request.Params["Direction"].Equals("Up"))
                {
                    Util.MoveSectionUp(Request.Params[_SiteUrl], Request.Params[_ListId], Request.Params[_SelectedItemId], Request.Params[_Folder]);
                }
                else
                {
                    Util.MoveSectionDown(Request.Params[_SiteUrl], Request.Params[_ListId], Request.Params[_SelectedItemId], Request.Params[_Folder]);
                }
                //Close the Page...
                CloseDialog();
            }
            catch (Exception ex)
            {
                //Log it to the ULS...
                HandleException(ex, "MoveSection.PageLoad");
                //Let the user know...
                ShowException(ex);
            }
        }
        #endregion

        #region Show Exception
        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="ex"></param>
        private void ShowException(Exception ex)
        {
            ErrorDiv.InnerText = ex.Message;
            ErrorDiv.Visible = true;
        }
        #endregion
    }
}
