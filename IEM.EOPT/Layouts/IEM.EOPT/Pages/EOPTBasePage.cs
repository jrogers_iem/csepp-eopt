﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using IEM.EOPT.Util;

namespace IEM.EOPT.Layouts.IEM.EOPT.Pages
{
    public class EOPTBasePage : LayoutsPageBase
    {       

        #region Constants
        //Define the QueryString Params that get used...
        protected const string _SiteUrl = "SiteUrl";
        protected const string _WebUrl = "WebUrl";
        protected const string _ListId = "ListId";
        protected const string _ItemId = "ItemId";
        protected const string _Folder = "Folder";
        protected const string _SelectedItemId = "SelectedItemId";       
             
        #endregion
        
        #region Constructor
        internal EOPTBasePage()
        {
            base.CheckRights();
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Get the selected SPList based on the QueryString paramaeters.
        /// </summary>
        /// <returns></returns>
        protected SPList GetList()
        {
            //Get the Site...
            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {   //Get the Web...
                using (SPWeb Web = Site.OpenWeb())
                {   //Get the List...
                    return Web.Lists[new Guid(Request.Params[_ListId])];
                }
            }
        }
        /// <summary>
        /// Gets the selected SPListItem based on the QueryString parameters.
        /// </summary>
        /// <returns></returns>
        protected SPListItem GetSelectedItem()
        {   //Get the Site...
            using (SPSite Site = new SPSite(Request.Params[_SiteUrl]))
            {   //Get the Web...
                using (SPWeb Web = Site.OpenWeb())
                {   //Get the List...
                    SPList List = Web.Lists[new Guid(Request.Params[_ListId])];
                    //Return the Item...
                    return List.GetItemByIdAllFields(Int32.Parse(Request.Params[_SelectedItemId]));
                }
            }
        }
        /// <summary>
        /// Closes the modal dialog.
        /// </summary>
        protected void CloseDialog()
        {
            Context.Response.Write("<script type='text/javascript'>window.frameElement.commitPopup();</script>");
            Context.Response.Flush();
            Context.Response.End();
        }
        #endregion

        #region HandleException
        /// <summary>
        /// Writes the exception to the SharePoint ULS logs.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="Source"></param>
        protected void HandleException(Exception ex, String Source)
        {
            BaseUtil Util = new BaseUtil();

            Util.HandleException(ex, Source);
        }
        #endregion
    }
}
