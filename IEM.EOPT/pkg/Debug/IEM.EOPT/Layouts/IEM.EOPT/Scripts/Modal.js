﻿
 $(document).ready(function(){
     //In SharePoint OOTB list view, all 'td' will have this class 'ms-cellstyle'
     $("td.ms-cellstyle a").click(function () {
     //$("td.ms-vb2 a").click(function () {
         var currentURL = $(this).attr('href'); 
         var onclickVal = $(this).attr('onclick') || '';
         if(onclickVal == '') {
             currentURL = "javascript:ModalDialog('"+currentURL+"')";           
             $(this).attr('onclick', 'empty');     
             $(this).attr('href', currentURL);        
         }
     });
 });
   

 function StatusDialog(urlvalue) {
     var options = {
         url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
         autoSize: true,
         allowMaximize: false,
         showClose: true,
         dialogReturnValueCallback: refreshCallback
     };
     SP.UI.ModalDialog.showModalDialog(options);
 }

 function ModalDialog(urlvalue) {     
     
    var options = {
        url: urlvalue,
        width: 800,        
        allowMaximize: true,
        showClose: true,     
        dialogReturnValueCallback: silentCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function silentCallback(dialogResult, returnValue) {
}
function refreshCallback(dialogResult, returnValue) {
    SP.UI.Notify.addNotification('Operation Complete!');
    SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
}

function AddSection(urlvalue, source) {
        
    var currentFolder = getUrlParameter('RootFolder');  

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue +'&Folder=' + currentFolder,
        autoSize:true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function RemoveSection(urlvalue, source) {

    if (confirm('Are you sure you want to send this item to the Recycle Bin?')) {
        var currentFolder = getUrlParameter('RootFolder');

        var options = {
            url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder,
            autoSize: true,
            allowMaximize: false,
            showClose: true,
            dialogReturnValueCallback: refreshCallback
        };
        SP.UI.ModalDialog.showModalDialog(options);
    }
}
function EditProperties(urlvalue) {    

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function MergeSection(urlvalue, source) {

    var currentFolder = getUrlParameter('RootFolder');

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder,
        height: 350,
        width: 450,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function ModifySection(urlvalue) {
    
    var currentFolder = getUrlParameter('RootFolder');

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder,        
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function MoveSectionDown(urlvalue, source) {

    var currentFolder = getUrlParameter('RootFolder');

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder + '&Direction=Down',
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function MoveSectionUp(urlvalue, source) {

    var currentFolder = getUrlParameter('RootFolder');

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder + '&Direction=Up',
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function RequestApproval(urlvalue) {
    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function SectionDetails(urlvalue) {
    
    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: silentCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function ToggleNIMS(urlvalue) {
        
    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}

function ConvertToPDF(urlvalue) {
    
    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
        autoSize: true,
        allowMaximize: false,
        showClose: true,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};



