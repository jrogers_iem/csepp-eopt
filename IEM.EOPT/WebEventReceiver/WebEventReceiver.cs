﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.SharePoint;

namespace IEM.EOPT.WebEventReceiver
{
    class WebEventReceiver : SPWebEventReceiver
    {
        public override void WebAdding(SPWebEventProperties properties)
        {
            base.WebAdding(properties);
        }
        public override void WebProvisioned(SPWebEventProperties properties)
        {
            base.WebProvisioned(properties);
        }
    }
}
